/* ALL THE SCRIPTS IN THIS FILE ARE MADE BY KROWNTHEMES.COM --- REDISTRIBUTION IS NOT ALLOWED! */
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload()
    }
});

$.ajaxSetup({
    cache: false
});

// CONTINUE WITH EVERYTHING ELSE

window.blockStickyHeader = false;

(function($) {

    var xDirection = "";
    var yDirection = "";

    var oldX = 0;
    var oldY = 0;

    var intervalRewind;

    function getMouseDirection(e) {

        //deal with the horizontal case
        if (oldX < e.pageX) {
            xDirection = "right";
        } else if (oldX > e.pageX) {
            xDirection = "left";
        } else if (oldX == e.pageX) {
            xDirection = xDirection;
        }

        // deal with the vertical case
        if (oldY <= e.pageY) {
            yDirection = "down";
        } else {
            yDirection = "up";
        }

        oldX = e.pageX;
        oldY = e.pageY;

        // console.log(xDirection);

        return (xDirection);

    }

    // $('.with-video').mousemove(function(e) {
    // 	var player = $(this).find('.video-js').attr('id');
    // 	var direction = getMouseDirection(e);
    //   var video = videojs(player);
    // 	if (direction == 'right') {
    // 		videojs(player).play();
    //
    // 	} else {
    //
    // 		video.pause();
    // 		var fps = 24;
    // 		var current = video.currentTime();
    //     var intervalRewind = setInterval(function() {
    //         if(current == 0){
    //            clearInterval(intervalRewind);
    //            video.pause();
    //         }
    //         else {
    //             video.currentTime(current += -(1/fps));
    //         }
    //     }, 1000 / fps);
    // 	}
    // });

    $('#add-to-cart.bundle-button').on('click', function(e) {
        e.preventDefault();
        console.log('bundle button!');
        var diamondData = $(this).attr('data-diamond');
        var ringData = $(this).attr('data-ring');

        Shopify.queue = [];

        if (diamondData != '') {
            Shopify.queue.push({
                variantId: diamondData,
                quantity: 1
            });
        }

        if (ringData != '') {
            Shopify.queue.push({
                variantId: ringData,
                quantity: 1
            });
        }

        Shopify.moveAlong = function() {
            if (Shopify.queue.length) {
                var request = Shopify.queue.shift();
                Shopify.addItem(request.variantId, request.quantity, Shopify.moveAlong);
            } else {
                console.log('finished!');
                document.location.href = '/cart';
                // window.sidebarCartAjaxFunctions();
            }
        };

        Shopify.moveAlong();

        // console.log($(this).attr('data-diamond'));
        // console.log($(this).attr('data-ring'));
        // if ($(this).attr('data-ring') == '') {
        //   console.log('data-ring is empty');
        // }

    });


    $('#build-your-ring-button').on('click', function(e) {

        if ($(this).attr('data-type') == 'is-ring') {

            // check if ring should be first step

            if (Cookies.get('first_step') != 'diamond' && Cookies.get('first_step') != undefined) {
                Cookies.set('first_step', 'ring');
            }

            Cookies.set('ring_collection', $(this).attr('data-collection'));
            Cookies.set('ring_handle', $(this).attr('data-handle'));
            Cookies.set('ring_variant', $(this).attr('data-variant'));
            if ($(this).attr('data-video') != '' || $(this).attr('data-video') != undefined) {
                Cookies.set('ring_video', $(this).attr('data-video'));
            }

        } else if ($(this).attr('data-type') == 'is-diamond') {

            if (Cookies.get('first_step') != 'ring' && Cookies.get('first_step') != undefined) {
                Cookies.set('first_step', 'diamond');
            }

            Cookies.set('diamond_collection', $(this).attr('data-collection'));
            Cookies.set('diamond_handle', $(this).attr('data-handle'));
            if ($(this).attr('data-video') != '' || $(this).attr('data-video') != undefined) {
                Cookies.set('diamond_video', $(this).attr('data-video'));
            }

        }

    });

    var resetCookies = function() {
        Cookies.remove('first_step');
        Cookies.remove('diamond_id');
        Cookies.remove('diamond_handle');
        Cookies.remove('diamond_title');
        Cookies.remove('diamond_price');
        Cookies.remove('diamond_image');
        Cookies.remove('diamond_video');
        Cookies.remove('ring_handle');
        Cookies.remove('ring_variant');
        Cookies.remove('ring_title');
        Cookies.remove('ring_price');
        Cookies.remove('ring_image');
        Cookies.remove('ring_alt');
        Cookies.remove('ring_video');
        Cookies.remove('ring_style');
        Cookies.remove('ring_size');
        console.log('cookies removed');
    }

    // resetCookies();


    // fix title widows

    $(".title").each(function() {
        var wordArray = $(this).text().split(" ");
        if (wordArray.length > 1) {
            wordArray[wordArray.length - 2] += "&nbsp;" + wordArray[wordArray.length - 1];
            wordArray.pop();
            $(this).html(wordArray.join(" "));
        }
    });

    $(".widow-fixed p").each(function() {
        var wordArray = $(this).text().split(" ");
        if (wordArray.length > 1) {
            wordArray[wordArray.length - 2] += "&nbsp;" + wordArray[wordArray.length - 1];
            wordArray.pop();
            $(this).html(wordArray.join(" "));
        }
    });

    $(".box__landing-banner p.details").each(function() {
        var wordArray = $(this).text().split(" ");
        if (wordArray.length > 1) {
            wordArray[wordArray.length - 2] += "&nbsp;" + wordArray[wordArray.length - 1];
            wordArray.pop();
            $(this).html(wordArray.join(" "));
        }
    });

    // style video background
    $(".video-js").css("background-color", "#ffffff");

    // play video on hover

    $(".with-video").hover(
        function() {
            var player = $(this).find('.video-js').attr('id');
            // console.log(player);
            videojs(player).play();
        },
        function() {
            var player = $(this).find('.video-js').attr('id');
            videojs(player).pause();
        }
        );

    $('#site-filters select:not(.styled)').each(function() {

        $(this).styledSelect({
            coverClass: 'regular-select-cover',
            innerClass: 'regular-select-inner'
        }).addClass('styled');

        $(this).parent().append($.themeAssets.arrowDown);

        if ($(this).data('label') != '') {

            if ($(this).find('option:selected').hasClass('default')) {
                $(this).parent().find('.regular-select-inner').html($(this).data('label'));
            }

            $(this).on('change', function(e) {
                if ($(this).find('option:selected').hasClass('default')) {
                    $(this).parent().find('.regular-select-inner').html($(this).data('label'));
                }
            });

        }

    });

    $('.site-header').append('<span id="js-helpers"><span id="fix-me-header"></span><span id="fix-me-collection"></span></span>');

    // tab navigation

    $('a:not(.ach), button, span, input').on('focus', function(e) {
        $(this).addClass('hover');
    }).on('blur', function(e) {
        $(this).removeClass('hover');
    });

    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $('.site-close-handle').trigger('click');
        }
    });

    // We start with the newsletter code (it needs to be wrapped inside a $(window).load() event function in order to get the perfect timeout after the site has completely loaded

    window.CUBER = {

        // MAIN

        Nav: {

            $siteHeader: null,
            $siteNav: null,
            $siteOverlay: null,

            mount: function() {

                this.$siteHeader = $('#site-header');
                this.$siteNav = $('#site-nav--mobile');
                this.$siteOverlay = $('#site-overlay');

                $('#site-menu-handle').on('click focusin', (function() {

                    if (!this.$siteNav.hasClass('active')) {

                        this.$siteNav.addClass('active');
                        this.$siteNav.removeClass('show-filters').removeClass('show-cart').removeClass('show-search');

                        this.$siteOverlay.addClass('active');

                        $('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
                        $('body').addClass('sidebar-move');

                    }

                }).bind(this));


                // if ($('#build-your-ring-button').length > 0) {
                //
                //   $('#build-your-ring-button').on('click', (function() {
                //
                //     if ( ! this.$siteNav.hasClass('active') ) {
                //
                //     	this.$siteNav.addClass('active');
                //     	this.$siteNav.removeClass('show-cart').removeClass('show-search').addClass('show-filters');
                //
                //     	this.$siteOverlay.addClass('active');
                //
                //     	$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
                //     	$('body').addClass('sidebar-move');
                //
                //     }
                //
                //
                //   }).bind(this));
                //
                // }


                if ($('#site-filter-handle').length > 0) {

                    $('#site-filter-handle').on('click', (function() {

                        if (!$('.pt-mobile-header').hasClass('active')) {
                            $('.pt-mobile-header').addClass('active');
                        } else {
                            $('.pt-mobile-header').removeClass('active');
                        }

                        // if ( ! this.$siteNav.hasClass('active') ) {
                        //
                        // 	this.$siteNav.addClass('active');
                        // 	this.$siteNav.removeClass('show-cart').removeClass('show-search').addClass('show-filters');
                        //
                        // 	this.$siteOverlay.addClass('active');
                        //
                        // 	$('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
                        // 	$('body').addClass('sidebar-move');
                        //
                        // }

                    }).bind(this));

                }

                if ($('#site-cart-handle').length > 0 && !$('html').hasClass('ie9')) {

                    if ($.themeCartSettings == 'overlay') {

                        $('#site-cart-handle a').addClass('block-fade');

                        $('#site-cart-handle a').on('click', (function(e) {

                            e.preventDefault();

                            if (!this.$siteNav.hasClass('active')) {

                                this.$siteNav.addClass('active');
                                this.$siteNav.removeClass('show-filters').removeClass('show-search').addClass('show-cart');

                                this.$siteOverlay.addClass('active');

                                $('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
                                $('body').addClass('sidebar-move');

                            }

                        }).bind(this));

                    }

                    $('#site-cart-handle').on('mouseenter focusis', function() {
                        $(this).addClass('hover-in').removeClass('hover-out');
                    }).on('mouseleave focusout', function() {
                        $(this).addClass('hover-out').removeClass('hover-in');
                    });

                }

                if ($('#site-search-handle').length > 0) {

                    $('#site-search-handle a').addClass('block-fade');

                    $('#site-search-handle a').on('click', (function(e) {

                        e.preventDefault();

                        if (!this.$siteNav.hasClass('active')) {

                            if ($('html').hasClass('ie9')) {
                                document.location.href = '/search';
                            } else {
                                this.$siteNav.addClass('active');
                                this.$siteNav.removeClass('show-filters').removeClass('show-cart').addClass('show-search');

                                this.$siteOverlay.addClass('active');

                                $('.fixing-scroll-now .site-box-background').addClass('sidebar-move');
                                $('body').addClass('sidebar-move');

                                $('form.search-bar input[type="search"]').focus();
                            }

                        }

                    }).bind(this));

                    $('#site-search-handle').on('mouseenter', function() {
                        $(this).addClass('hover-in').removeClass('hover-out');
                    }).on('mouseleave focusout', function() {
                        $(this).addClass('hover-out').removeClass('hover-in');
                    });

                    $('a[href="#search"]').on('click', function(e) {
                        e.preventDefault();
                        $('#site-search-handle a').trigger('click');
                    })

                }

                $('.site-close-handle, #site-overlay').on('click', (function() {

                    if (this.$siteNav.hasClass('active')) {

                        this.$siteNav.removeClass('active');
                        this.$siteOverlay.removeClass('active');

                        $('.fixing-scroll-now .site-box-background').removeClass('sidebar-move');
                        $('body').removeClass('sidebar-move');

                    }

                }).bind(this));

                ///

                $('.site-nav.style--classic .has-submenu').each(function() {
                    $(this).on('mouseenter focusin', function() {
                        $(this).find('.submenu').stop().slideDown(200);
                        $('body').addClass('opened-submenu');
                        $('body').addClass('opened-submenu-flag');
                        $('.site-overlay').addClass('active');
                        $(this).find('.submenu').attr('aria-expanded', 'true');
                    }).on('mouseleave focusout', function() {
                        $(this).find('.submenu').attr('aria-expanded', 'false');
                        $(this).find('.submenu').stop().slideUp(200, function() {
                            if (!$('body').hasClass('opened-submenu-flag')) {
                                $('body').removeClass('opened-submenu');
                            }
                        });
                        $('.site-overlay').removeClass('active');
                        $('body').removeClass('opened-submenu-flag');
                    })
                })

                $('.site-nav.style--sidebar .has-submenu:not(.collections-menu)').each(function() {

                    $(this).children('a').addClass('block-fade');
                    $(this).children('a').on('click touchstart', function(e) {
                        e.preventDefault();
                    })

                    $(this).children('a').on('click touchstart', function(e) {

                        e.preventDefault();
                        var $parent = $(this).parent();

                        if ($parent.hasClass('active')) {

                            var tempBoNo = false;
                            var tempBoHref = $(this).attr('href');

                            if (tempBoHref != '') {

                                if (tempBoHref.indexOf('#') >= 0) {

                                    tempBoNo = true;

                                    if (tempBoHref.indexOf('#') == (tempBoHref.length - 1)) {
                                        // nothing
                                    } else {
                                        $('.site-close-handle').trigger('click');
                                        document.location.href = tempBoHref;
                                    }

                                } else if ($(this).attr('target') == '_blank') {

                                    window.open(tempBoHref, '_blank');

                                } else {

                                    $('body').fadeOut(200);
                                    setTimeout((function() {
                                        document.location.href = tempBoHref;
                                    }).bind(this), 200);
                                    e.preventDefault();

                                }

                            } else {
                                tempBoNo = true;
                            }

                            if (tempBoNo) {
                                $parent.find('.submenu').stop().slideUp(200);
                                $parent.removeClass('active');
                                $parent.find('.submenu').attr('aria-expanded', 'false');
                            }

                        } else {
                            $parent.addClass('active');
                            $parent.find('.submenu').stop().slideDown(200);
                            $parent.find('.submenu').attr('aria-expanded', 'true');
                        }

                        $(this).removeClass('hover');

                    })

                })

                $('.site-nav.style--sidebar .has-babymenu:not(.collections-menu)').each(function() {

                    $(this).children('a').addClass('block-fade');
                    $(this).children('a').on('click touchstart', function(e) {
                        e.preventDefault();
                    })

                    $(this).children('a').on('click touchstart', function(e) {

                        e.preventDefault();
                        var $parent = $(this).parent();

                        if ($parent.hasClass('active')) {

                            var tempBoNo = false;
                            var tempBoHref = $(this).attr('href');

                            if (tempBoHref != '') {

                                if (tempBoHref.indexOf('#') >= 0) {

                                    tempBoNo = true;

                                    if (tempBoHref.indexOf('#') == (tempBoHref.length - 1)) {
                                        // nothing
                                    } else {
                                        $('.site-close-handle').trigger('click');
                                        document.location.href = tempBoHref;
                                    }

                                } else if ($(this).attr('target') == '_blank') {

                                    window.open(tempBoHref, '_blank');

                                } else {

                                    $('body').fadeOut(200);
                                    setTimeout((function() {
                                        document.location.href = tempBoHref
                                    }).bind(this), 200);
                                    e.preventDefault();

                                }

                            } else {
                                TempBoNo = true;
                            }

                            if (tempBoNo) {
                                $parent.removeClass('active');
                                $parent.find('.babymenu').stop().slideUp(200);
                                $parent.find('.babymenu').attr('aria-expanded', 'false');
                            }

                        } else {
                            $parent.addClass('active');
                            $parent.find('.babymenu').stop().slideDown(200);
                            $parent.find('.babymenu').attr('aria-expanded', 'true');
                        }

                    })

                })

                $('.style--classic .babymenu').each(function() {

                    var bestWidth = 0;
                    $(this).parent().parent().css('display', 'block');

                    $(this).find('a').each(function() {
                        $(this).css('position', 'fixed');
                        $(this).attr('data-width', $(this).outerWidth(true))
                        if ($(this).outerWidth() > bestWidth) {
                            bestWidth = $(this).outerWidth(true);
                        }
                        $(this).css({
                            'position': 'static',
                            'width': '100%'
                        });
                    });

                    bestWidth += 30;
                    $(this).css('width', bestWidth);
                    $(this).css('transform', 'translateX(-45%)');
                    $(this).parent().parent().css('display', 'none');
                    $(this).css('display', 'none');

                });

                $('.style--classic .has-babymenu').each(function() {
                    $(this).on('mouseenter focusin', function() {
                        $(this).find('.babymenu').stop().slideDown(200);
                        $(this).find('.babymenu ul').attr('aria-expanded', 'true');
                        $(this).css('zIndex', 9);
                    }).on('mouseleave focusout', function() {
                        $(this).find('.babymenu').stop().slideUp(200);
                        $(this).find('.babymenu ul').attr('aria-expanded', 'false');
                        $(this).css('zIndex', 1);
                    });
                });

                ///

                $('body').addClass('desktop--add-some-padding');

                $('.style--classic li.has-submenu[data-size]').each(function() {
                    var menuSize = parseInt($(this).data('size'))
                    if (menuSize > 15) {
                        $(this).addClass('text-smallest');
                    } else if (menuSize > 10) {
                        $(this).addClass('text-smaller');
                    } else if (menuSize > 5) {
                        $(this).addClass('text-small');
                    }
                });

                // end -- */

                if ($('#site-header').hasClass('header-scroll')) {

                    if ($('body').hasClass('template-index')) {
                        $('body').addClass('index-margin-fix');
                    }

                    window.lst = $(window).scrollTop();
                    // console.log('scroll?');

                    $('.site-nav.style--classic .submenu').css('top', $('.site-header').outerHeight())

                    $(window).on('scroll.sticky-header', (function() {

                        if (!window.blockStickyHeader) {

                            var st = $(window).scrollTop();

                            if (st < 0 || Math.abs(lst - st) <= 5)
                                return;

                            if (st == 0) {

                                this.$siteHeader.removeClass('animate');
                                this.$siteHeader.removeClass('fix');
                                this.$siteHeader.removeClass('ready');
                                //$('body').css('paddingTop', 0);

                            } else if (st <= lst && !this.$siteHeader.hasClass('fix')) {

                                this.$siteHeader.addClass('fix');
                                //$('body').css('paddingTop', this.$siteHeader.outerHeight());
                                setTimeout((function() {
                                    this.$siteHeader.addClass('ready');
                                }).bind(this), 5);
                                setTimeout((function() {
                                    this.$siteHeader.addClass('animate');
                                }).bind(this), 25);

                            } else if (st > lst && this.$siteHeader.hasClass('animate')) {

                                this.$siteHeader.removeClass('animate');
                                setTimeout((function() {
                                    this.$siteHeader.removeClass('fix');
                                    this.$siteHeader.removeClass('ready');
                                    //$('body').css('paddingTop', 0);
                                }).bind(this), 105);

                            }

                            window.lst = st;
                            // console.log('triggered?');
                        }

                    }).bind(this));

                }

                // Ajax cart functions (in the sidebar)

                window.sidebarCartAjaxFunctions();

            },

            unmount: function() {

                $('#site-menu-handle').off('click');
                $('#site-cart-handle a').off('click');
                $('#site-filter-handle').off('click');

                this.$siteNav.removeClass('active');
                this.$siteOverlay.removeClass('active');

                $('.fixing-scroll-now .site-box-background').removeClass('sidebar-move');
                $('body').removeClass('sidebar-move');

                $(window).off('scroll.sticky-header');

            }

        },

        // COLLECTION

        Collection: {

            $collectionGrid: null,
            $collectionNext: null,
            $collectionNextLink: null,
            $filterMenuLinks: null,
            $sortCaratAscButton: null,
            $sortCaratDescButton: null,
            $diamondRows: null,
            $body: null,
            $sortedByCaratAsc: false,
            $sortedByCaratDesc: false,
            _first_step: null,

            mount: function() {

                // check if user is building a ring
                var $collection = $('#section-collection');

                if ($collection.hasClass('build_ring')) {

                    var diamondParam = this._getParameterByName('diamond');
                    var diamondCookie = Cookies.get('diamond_handle');
                    var ringParam = this._getParameterByName('ring');
                    var ringCookie = Cookies.get('ring_handle');
                    var styleParam = this._getParameterByName('style');
                    var variantCookie = Cookies.get('ring_variant');
                    var diamond_handle;
                    var ring_handle;
                    var ring_style;
                    var $submit = $collection.find('.atc-button');
                    var ringPrice;
                    var diamondPrice;

                    console.log('diamondCookie: ' + diamondCookie);
                    console.log('ringCookie: ' + ringCookie);

                    function loadProductJson(param, type, step) {
                        console.log('loadProductJson');
                        $.ajax({
                            url: '/products/' + param + '?view=json',
                            type: 'GET'
                        })
                        .done(function(data) {
                            console.log('type: ' + type);
                            console.log('loadProductJson done for ' + type + '...');
                            var productData = $.parseJSON(data);

                            console.log(productData);
                            console.log(productData.metafields);

                            if (type == 'diamond') {
                                var productId, productTitle, productPrice, productImage, productVideo, productStyle, productSize, productUrl, collectionUrl;

                                productId = productData.product.data.variants[0].id;
                                productTitle = productData.product.data.title;
                                productPrice = productData.product.data.price;
                                productImage = productData.product.data.featured_image;
                                productVideo = productData.metafields.video_file;
                                productStyle = null;
                                productSize = null;

                                    // diamondPrice = productPrice;

                                    // console.log(productImage);
                                    Cookies.set('diamond_id', productId);
                                    Cookies.set('diamond_handle', diamond_handle);
                                    Cookies.set('diamond_title', productTitle);
                                    Cookies.set('diamond_price', productPrice);
                                    Cookies.set('diamond_image', productImage);
                                    Cookies.set('diamond_video', productVideo);

                                    if (step == 1) {
                                        console.log('put diamond in first box');
                                        Cookies.set('first_step', 'diamond');
                                    } else {
                                        console.log('put diamond in second box');
                                    }

                                    collectionUrl = '/collections/all-lab-grown-diamonds/?diamond=' + diamond_handle + '&ring=' + ring_handle + '&style=' + ring_style;
                                    productUrl = '/collections/all-lab-grown-diamonds/products/' + diamond_handle + '?ring=' + ring_handle + '&style=' + ring_style;

                                    if (productTitle && productTitle != undefined) {
                                        $('#build-wrapper').removeClass('is-hidden');
                                        buildStep(
                                            'diamond',
                                            step,
                                            productId,
                                            productTitle,
                                            productPrice,
                                            productImage,
                                            productAlt,
                                            productVideo,
                                            productStyle,
                                            productSize,
                                            productUrl,
                                            collectionUrl
                                            );
                                    }

                                } else if (type == 'ring') {

                                    // console.log('styleParam: ' + styleParam);
                                    var productId, productTitle, productPrice, productImage, productAlt, productVideo, productStyle, productSize, productUrl, collectionUrl;

                                    // console.log('styleParam:' + styleParam);
                                    // console.log('style Cookie:' + Cookies.get('ring_variant'));

                                    var variantStyle = (styleParam != undefined) ? styleParam : Cookies.get('ring_variant');

                                    // console.log('variantStyle ' + variantStyle);
                                    // console.log(productData.product.data);

                                    $.each(productData.product.data.variants, function(key, value) {
                                        if (value.id == variantStyle) {
                                            console.log('match!');
                                            productId = variantStyle;
                                            productTitle = productData.product.data.title;
                                            productPrice = value.price;

                                            if (value.featured_image != null) {
                                                productImage = value.featured_image.src;
                                                productAlt = value.featured_image.alt;
                                            } else {
                                                productImage = productData.product.data.featured_image;
                                                productAlt = productData.product.data.title;
                                            }

                                            productVideo = productData.metafields.video_file;
                                            productStyle = value.option1;
                                            productSize = value.option2;
                                            return false;
                                        }
                                    });

                                    // ringPrice = productPrice;

                                    // console.log(productImage);

                                    Cookies.set('ring_handle', ring_handle);
                                    Cookies.set('ring_variant', variantStyle);
                                    Cookies.set('ring_title', productTitle);
                                    Cookies.set('ring_price', productPrice);
                                    Cookies.set('ring_image', productImage);
                                    Cookies.set('ring_alt', productAlt);
                                    Cookies.set('ring_video', productVideo);
                                    Cookies.set('ring_style', productStyle);
                                    Cookies.set('ring_size', productSize);

                                    if (step == 1) {
                                        // console.log('put ring in first box');
                                        Cookies.set('first_step', 'ring');
                                    } else {
                                        // console.log('put ring in second box');
                                    }

                                    collectionUrl = '/collections/engagement-ring-settings/?ring=' + ring_handle + '&variant=' + ring_style + '&diamond=' + diamond_handle;
                                    productUrl = '/collections/engagement-ring-settings/products/' + ring_handle + '?variant=' + ring_style + '&diamond=' + diamond_handle;

                                    if (productTitle && productTitle != undefined) {
                                        $('#build-wrapper').removeClass('is-hidden');
                                        buildStep(
                                            'ring',
                                            step,
                                            productId,
                                            productTitle,
                                            productPrice,
                                            productImage,
                                            productAlt,
                                            productVideo,
                                            productStyle,
                                            productSize,
                                            productUrl,
                                            collectionUrl
                                            );
                                    }
                                }

                            })
.fail(function(data) {
    console.log('loadProductJson failed');
})
.always(function(data) {

});
}

function buildStep(
    type,
    step_value,
    productId,
    productTitle,
    productPrice,
    productImage,
    productAlt,
    productVideo,
    productStyle,
    productSize,
    productUrl,
    collectionUrl
    ) {
                        // console.log('type: ' + type);
                        if (productTitle && productTitle != undefined) {
                           var $section = $('#section-collection');
                           var $box = (type == 'diamond') ? $('#step-1') : $('#step-2');
                           var $links = (type == 'diamond') ? $box.find('a.step-1') : $box.find('a.step-2');
                           var $vewLink = $box.find('.view-link');
                           var $changeLink = $box.find('.change-link');
                           var $removeItemData = $box.find('.remove-item-data');
                           var $selectLink = $box.find('.select-link');
                           var $title = $box.find('h2.title');
                           var $caption = $box.find('p.caption a');
                           var $videoBox = $box.find('.with-video');
                           var $imageBox = $box.find('.no-video');
                           var $lpPlayer = $box.find('.lp-player')
                           var $img = $('<img>');
                           var s3url = $lpPlayer.attr('data-url');
                           var titleCopy = (type == 'diamond') ? 'Your<br />Diamond' : 'Your<br />Setting';
                           var priceFormatted;

                           $links.attr('href', productUrl);
                           $title.html(titleCopy);
                           $vewLink.attr('href', productUrl);
                           $vewLink.removeClass('is-hidden');
                           $removeItemData.removeClass('is-hidden');



                           $box.find('.is-fpo').addClass('is-hidden');

                           priceFormatted = '' + productPrice;
                           priceFormatted = '$' + priceFormatted.substring(0, priceFormatted.length - 2);

                           if (type == 'ring') {
                               productTitle = '<span class="product">' + productTitle + '</span><span class="detail">' + productStyle + '</span> <span class="detail">Size ' + productSize + '</span><span class="detail price">' + priceFormatted + '</span>';
                               ringPrice = productPrice;

                               if ($section.hasClass('build-collection-diamond')) {
                                   console.log('we are on a diamond page, and this is a ring, so add a change button');
                                   $changeLink.attr('href', collectionUrl);
                                   $changeLink.removeClass('is-hidden');
                               } else if ($section.hasClass('build-collection-ring')) {
	                                // consoe.log('we are on a ring page, and this is a ring, so hide the change button');
	                            }

	                        } else {
                               productTitle = '<span class="product">' + productTitle + '</span><span class="detail price">' + priceFormatted + '</span>';
                               diamondPrice = productPrice;

                               if ($section.hasClass('build-collection-diamond')) {
	                                // console.log('we are on a diamond page, and this is a diamond, so hide the change button');
	                            } else if ($section.hasClass('build-collection-ring')) {
	                                // consoe.log('we are on a ring page, and this is a diamond, so show the change button');
	                                $changeLink.attr('href', collectionUrl);
	                                $changeLink.removeClass('is-hidden');
	                            }
	                        }

	                        $caption.html(productTitle);
	                        $img.addClass('lazyload');
	                        $img.attr('src', productImage);

	                        if (productVideo != null) {
                               console.log(productTitle + ' has video');
                               var videoPlayer = document.createElement('video');
                               $(videoPlayer).addClass('video-js');

                               $lpPlayer.append(videoPlayer);

                               videojs(videoPlayer, {
                                   muted: true,
                                   loop: true,
                                   preload: 'auto'
                               });

                               videojs(videoPlayer).src({
                                   type: 'video/mp4',
                                   src: s3url + productVideo
                               });

                               $imageBox.addClass('video-fallback');
                               $imageBox.append($img);
                               $videoBox.addClass('active');
                               $videoBox.removeClass('is-hidden')

                           } else {
                               console.log(productTitle + ' has no video');
                               $imageBox.append($img);
                               $imageBox.addClass('active');
                               $imageBox.removeClass('is-hidden');

                           }

                           if (step_value == 1) {
                               $('.atc-button').attr('data-product-1', productId);
                               if (type == 'ring') {
                                   console.log('this is step 1, and is a ring');
                               } else {
                                   console.log('this is step 1, and is a diamond');
                               }
                           } else {
                               if (type == 'ring') {
                                   console.log('this is step 2, and is a ring');
                               } else {
                                   console.log('this is step 2, and is a diamond');
                               }
                               $('.atc-button').attr('data-product-2', productId);
                           }

                           checkAffirm();
                       }
                   }

                    // check params first

                    if (diamondParam != null && diamondParam != 'null' && diamondParam != undefined) {

                        console.log('diamond param found');

                        diamond_handle = diamondParam;

                        if (Cookies.get('first_step') != undefined) {
                            var step = (Cookies.get('first_step') == 'diamond') ? 1 : 2;
                        } else {
                            var step = ($('.build-collection-diamond').length > 0) ? 1 : 2;
                        }
                        loadProductJson(diamondParam, 'diamond', step);
                    } else {
                        // console.log('no diamond param found');

                        if (diamondCookie != undefined && diamondCookie != 'null') {
                            // console.log('diamond cookie found');
                            diamond_handle = diamondCookie;
                            if (Cookies.get('first_step') != undefined) {
                                var step = (Cookies.get('first_step') == 'diamond') ? 1 : 2;
                            } else {
                                var step = ($('.build-collection-diamond').length > 0) ? 1 : 2;
                            }
                            loadProductJson(diamondCookie, 'diamond', step);
                        } else {
                            // buildTempDiamond();

                            // console.log('no diamond cookie found');
                        }

                    }

                    if (ringParam != null && ringParam != 'null' && ringParam != undefined) {

                        console.log('ring param found');

                        ring_handle = ringParam;
                        ring_style = styleParam;

                        if (Cookies.get('first_step') != undefined) {
                            var step = (Cookies.get('first_step') == 'ring') ? 1 : 2;
                        } else {
                            var step = ($('.build-collection-ring').length > 0) ? 1 : 2;
                        }
                        loadProductJson(ringParam, 'ring', step);
                    } else {
                        // console.log('no ring param found');
                        if (ringCookie != undefined && ringCookie != 'null') {
                            // console.log('ring cookie found');
                            ring_handle = ringCookie;
                            ring_style = variantCookie;
                            if (Cookies.get('first_step') != undefined) {
                                var step = (Cookies.get('first_step') == 'ring') ? 1 : 2;
                            } else {
                                var step = ($('.build-collection-ring').length > 0) ? 1 : 2;
                            }
                            loadProductJson(ringCookie, 'ring', step);
                        } else {
                            // console.log('no ring cookie found');

                        }

                    }

                    function buildTempDiamond() {
                        // var $imageBox = $box.find('.no-video');
                        // var $img = $('<img>');
                        // $img.attr('src', )
                    }


                    // affirm

                    function checkAffirm() {
                        console.log('checkAffirm called');
                        console.log('diamondPrice: ' + diamondPrice);
                        console.log('ringPrice: ' + ringPrice);
                        // console.log('diamond_handle: ' + diamond_handle);
                        // console.log('ring_handle: ' + ring_handle);

                        if (diamond_handle != undefined && diamond_handle != 'null' && ring_handle != undefined && ring_handle != 'null') {

                            if (diamondPrice != undefined && ringPrice != undefined) {
                                console.log('checkAffirm activated');

                                var total_price = parseInt(diamondPrice, 10) + parseInt(ringPrice, 10);
                                var total_priceString = '' + total_price;
                                var totalPriceFormatted = '<p>$' + total_priceString.substring(0, total_priceString.length - 2) + '</p>';
                                var affirmText = '<p class="affirm-as-low-as" data-page-type="product" data-amount="' + total_price + '"></p>';
                                $('.ring__builder .column-1.total-price').append(totalPriceFormatted);
                                $('.ring__builder .column-2.affirm-price').append(affirmText);
                                affirm.ui.refresh();
                                $('.ring__builder.foot').removeClass('is-hidden');

                            }

                        }

                    }




                    Shopify.queue = [];

                    //
                    $submit.on('click', function(e) {
                        console.log('submit clicked');

                        Shopify.queue.push({
                            variantId: $(this).attr('data-product-1'),
                            quantity: 1
                        });

                        Shopify.queue.push({
                            variantId: $(this).attr('data-product-2'),
                            quantity: 1
                        });

                        Shopify.moveAlong = function() {
                            // if we still have requests in the queue, process the next one.
                            if (Shopify.queue.length) {
                                var request = Shopify.queue.shift();
                                Shopify.addItem(request.variantId, request.quantity, Shopify.moveAlong);
                            }
                            // if the queue is empty, redirect to the cart page.
                            else {
                                console.log('finished!');
                                document.location.href = '/cart';
                                // window.sidebarCartAjaxFunctions();
                            }
                        };
                        Shopify.moveAlong();

                    });

                }

                if ($('.diamond-box').length > 0) {
                    var sortCookie = Cookies.get('sort');

                    if (sortCookie != undefined) {

                        if (sortCookie == 'carat-asc') {

                            $('.diamond-box.box__collection').sort(function(a, b) {
                                return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                            }).insertAfter('.diamond-box-labels');

                            $('.filter-group-sort-orders').find('.menu-trigger').html('Carat: Low to High');

                            $('.nav-sort-orders li').each(function() {
                                $(this).removeClass('selected');
                                if ($(this).hasClass('carat-asc')) {
                                    $(this).addClass('selected');
                                }
                            });

                            $('body').addClass('sortedByCaratAsc');
                            $('body').removeClass('sortedByCaratDesc');
                            $('body').removeClass('sortedByPrice');

                            var i = 0;
                            $('.diamond-box.box__collection').each(function() {
                                $(this).removeClass('active');
                                setTimeout((function() {
                                    $(this).addClass('active');
                                }).bind(this), 10 * i++);
                            });

                        } else if (sortCookie == 'carat-desc') {

                            $('.diamond-box.box__collection').sort(function(a, b) {
                                return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                            }).reverse().insertAfter('.diamond-box-labels');

                            $('.filter-group-sort-orders').find('.menu-trigger').html('Carat: High to Low');

                            $('.nav-sort-orders li').each(function() {
                                $(this).removeClass('selected');
                                if ($(this).hasClass('carat-desc')) {
                                    $(this).addClass('selected');
                                }
                            });

                            $('body').removeClass('sortedByCaratAsc');
                            $('body').addClass('sortedByCaratDesc');
                            $('body').removeClass('sortedByPrice');

                            var i = 0;
                            $('.diamond-box.box__collection').each(function() {
                                $(this).removeClass('active');
                                setTimeout((function() {
                                    $(this).addClass('active');
                                }).bind(this), 10 * i++);
                            });

                        } else {

                            $('body').addClass('sortedByPrice');
                            $('body').removeClass('sortedByCaratAsc');
                            $('body').removeClass('sortedByCaratDesc');

                        }

                    }
                }

                if ($('.filter-menu').length > 0) {

                    this.$body = $('body');
                    this.$filterMenuLinks = $('.filter-menu a');
                    this.$sortCaratAscButton = $('a.sort-carat.weight-asc');
                    this.$sortCaratDescButton = $('a.sort-carat.weight-desc');
                    this.$diamondRows = $('.diamond-box:not(.diamond-box-labels).box__collection.active');


                    // sort by carat weight

                    $('body').on('click touchstart', '.sort-price', (function(e) {

                        $('body').removeClass('sortedByCaratAsc');
                        $('body').removeClass('sortedByCaratDesc');
                        $('body').addClass('sortedByPrice');

                        Cookies.set('sort', 'price');

                    }).bind(this));


                    $('body').on('click touchstart', '.sort-carat.weight-asc', (function(e) {

                        e.preventDefault();

                        Cookies.set('sort', 'carat-asc');

                        $('.diamond-box.box__collection').sort(function(a, b) {
                            return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                        }).insertAfter('.diamond-box-labels');

                        $('.filter-group-sort-orders').find('.menu-trigger').html('Carat: Low to High');

                        $('.nav-sort-orders li').each(function() {
                            $(this).removeClass('selected');
                            if ($(this).hasClass('carat-asc')) {
                                $(this).addClass('selected');
                            }
                        });

                        $('body').addClass('sortedByCaratAsc');
                        $('body').removeClass('sortedByCaratDesc');
                        $('body').removeClass('sortedByPrice');

                        this.$sortedByCaratAsc = true;
                        this.$sortedByCaratDesc = false;

                        var i = 0;
                        $('.diamond-box.box__collection').each(function() {
                            $(this).removeClass('active');
                            setTimeout((function() {
                                $(this).addClass('active');
                            }).bind(this), 10 * i++);
                        });

                    }).bind(this));



                    $('body').on('click touchstart', '.sort-carat.weight-desc', (function(e) {

                        e.preventDefault();

                        Cookies.set('sort', 'carat-desc');

                        $('.diamond-box.box__collection').sort(function(a, b) {
                            return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                        }).reverse().insertAfter('.diamond-box-labels');

                        $('.filter-group-sort-orders').find('.menu-trigger').html('Carat: High to Low');

                        $('.nav-sort-orders li').each(function() {
                            $(this).removeClass('selected');
                            if ($(this).hasClass('carat-desc')) {
                                $(this).addClass('selected');
                            }
                        });

                        $('body').removeClass('sortedByCaratAsc');
                        $('body').addClass('sortedByCaratDesc');
                        $('body').removeClass('sortedByPrice');

                        this.$sortedByCaratAsc = false;
                        this.$sortedByCaratDesc = true;

                        var i = 0;
                        $('.diamond-box.box__collection').each(function() {
                            $(this).removeClass('active');
                            setTimeout((function() {
                                $(this).addClass('active');
                            }).bind(this), 10 * i++);
                        });

                    }).bind(this));

                };

                // end need for cleanup code

                if ($('.box__paginate').length > 0) {

                    this.$collectionGrid = $('#section-collection');
                    this.$collectionNext = $('.box__paginate');
                    this.$collectionNextLink = $('.box__paginate a');

                    this.$collectionNextLink.append('<div class="preloader"><span>.</span><span>.</span><span>.</span></div>')

                    this.$collectionNextLink.on('click', (function(e) {

                        this.$collectionNextLink.addClass('loading');
                        var nextPageURL = this.$collectionNextLink.attr('href');

                        $.ajax({
                            url: nextPageURL
                        }).done((function(data) {

                            this.$collectionNextLink.removeClass('loading');
                            this.$collectionNext.before($(data).find('.site-box.box__collection'));
                            //window.CUBER.Images.mount();

                            // check if should sort by carat

                            var delay;

                            if ($('.diamond-box').length > 0) {

                                delay = 10;

                                if ($('body').hasClass('sortedByCaratAsc')) {

                                    this.$collectionGrid.find('.box__collection').sort(function(a, b) {
                                        return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                                    }).insertAfter('.diamond-box-labels');

                                    this.$collectionGrid.find('.box__collection').removeClass('active');


                                    console.log('sorted');

                                } else if ($('body').hasClass('sortedByCaratDesc')) {

                                    this.$collectionGrid.find('.box__collection').sort(function(a, b) {
                                        return ($(b).find('.box-weight').attr('data-weight')) < ($(a).find('.box-weight').attr('data-weight')) ? 1 : -1;
                                    }).reverse().insertAfter('.diamond-box-labels');

                                    this.$collectionGrid.find('.box__collection').removeClass('active');

                                    console.log('reveresed');

                                }

                            } else {
                                delay = 100;
                            }

                            var i = 0;
                            this.$collectionGrid.find('.box__collection:not(.active)').each(function() {

                                setTimeout((function() {
                                    $(this).addClass('active');
                                }).bind(this), delay * i++);
                            });

                            if ($(data).find('.site-box.box__paginate').length > 0) {
                                this.$collectionNextLink.attr('href', $(data).find('.site-box.box__paginate a').attr('href'));
                            } else {
                                this.$collectionNext.remove();
                            }

                        }).bind(this));

                        e.preventDefault();

                    }).bind(this));

                }

                $(window).on('resize.collection-grid', window.debounce((function() {
                    this._resizeCollection();
                }).bind(this), 300)).trigger('resize.collection-grid');
                this._resizeCollection();
                setTimeout((function() {
                    this._resizeCollection();
                }).bind(this), 1001);

                // empty grid fix

                var collectionSize = parseInt($('.box__heading').data('size'));

                if (collectionSize > 0 && collectionSize < 4) {
                    for (var i = 0; i < 4 - collectionSize; i++) {
                        $('#section-collection').append('<div class="site-box box--small box--typo-small lap--hide box--no-padding box__collection active" />');
                    }
                }

            },

            _buildProductStep: function(productType, productImage, productLink, step) {
                var box;
                var link = $('<a>', {
                    href: productLink
                });
                var img = $('<img class="product-image lazyload">');

                if (step == 1) {
                    box = $('#step-1');
                } else if (step == 2) {
                    box = $('#step-2');
                }

                if (productType == 'ring') {
                    img.attr('src', productImage);
                    link.append(img);
                    box.find('.product-image').append(link);
                } else if (productType == 'diamond') {

                }
            },

            _buildOptionsStep: function(options) {

            },

            _sortCaratAsc: function() {

                console.log('_sortCaratAsc called');

            },

            _resizeCollection: function() {


                if ($('#fix-me-header').css('display') == 'block' && $('#fix-me-collection').css('display') == 'block') {

                    var wh = $(window).height();
                    var h = wh - $('.site-header').outerHeight();

                    if (h > 720) {

                        //$('.mount-products .site-box.box__heading').attr('style', 'height:' + h + 'px !important; min-height: 0 !important;');
                        $('.mount-products .site-box.box__collection_image').attr('style', 'height:' + h + 'px !important; min-height: 0 !important;');

                    } else {

                        h = 720;

                        if ($('.mount-products .site-box.box__collection_image').length > 0) {
                            $('.mount-products .site-box.box__heading').attr('style', 'height:' + (wh - $('.site-header').outerHeight()) + 'px !important; min-height: 0 !important;');
                            $('.mount-products .site-box.box__collection_image').attr('style', 'height:' + (wh - $('.site-header').outerHeight()) + 'px !important; min-height: 0 !important;');
                        }


                    }

                } else {

                    $('.mount-products .site-box.box__collection, .mount-products .site-box.box__heading').attr('style', '');

                }

            },

            _getParameterByName: function(name) {

                var url = window.location.href;
                name = name.replace(/[\[\]]/g, '\\$&');
                var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';

                return decodeURIComponent(results[2].replace(/\+/g, ' '));

            },

            unmount: function() {
                $(window).off('resize.collection-grid');
            }

        },

        // PRODUCT

        Product: {

            $productGallery: null,
            $productGalleryItem: null,
            $productGalleryIndex: null,

            $productCarousel: null,
            $productCarouselImgs: null,
            productFlkty: null,

            $productStuff: null,
            productStuffMove: false,

            mount: function($product) {
                var $section = $('#section-product');
                var diamondParam, ringParam, styleParam;

                var diamondCookie = Cookies.get('diamond_handle');
                var ringCookie = Cookies.get('ring_handle');
                var variantCookie = Cookies.get('ring_variant');

                var diamond_handle;
                var ring_handle;
                var ring_style;

                var ringPrice;
                var diamondPrice;


                if ($section.hasClass('is-diamond') || $section.hasClass('is-ring')) {

                    if ($section.hasClass('is-diamond')) {
                        console.log('on a diamond page');

                        diamond_handle = $section.attr('data-handle');
                        ringParam = this._getParameterByName('ring');
                        styleParam = this._getParameterByName('style');
                        console.log('styleParam: ' + styleParam);

                        if (ringParam != null && ringParam != 'null' && ringParam != undefined && ringParam != 'undefined') {
                            console.log('we have a ring param');
                            ring_handle = ringParam;
                            ring_style = styleParam;
                            loadProductJson(ring_handle, 'ring');
                        } else {
                            console.log('no ring param, checking for cookie...');
                            if (ringCookie != undefined && ringCookie != 'null') {
                                console.log('...ring cookie detected!');
                                ring_handle = ringCookie;
                                ring_style = variantCookie;
                                loadProductJson(ringCookie, 'ring');
                            } else {
                                console.log('... no ring cookie found');
                            }
                        }

                    } else if ($section.hasClass('is-ring')) {
                        console.log('on a ring page');

                        ring_handle = $section.attr('data-handle');
                        ring_style = this._getParameterByName('variant');
                        diamondParam = this._getParameterByName('diamond');
                        // console.log('diamondParam: ' + diamondParam);

                        if (diamondParam != null && diamondParam != 'null' && diamondParam != undefined && diamondParam != 'undefined') {
                            console.log('we have a diamond param');
                            diamond_handle = diamondParam;
                            loadProductJson(diamond_handle, 'diamond');
                        } else {
                            console.log('no diamond param, checking for cookie...');
                            if (diamondCookie != undefined && diamondCookie != 'null') {
                                console.log('...diamond cookie detected!');
                                diamond_handle = diamondCookie;
                                loadProductJson(diamondCookie, 'diamond');
                            } else {
                                console.log('... no diamond cookie found');
                            }
                        }

                    }


                    console.log('diamond_handle: ' + diamond_handle);
                    console.log('ring_handle: ' + ring_handle);

                }

                function loadProductJson(param, type) {
                    console.log('loadProductJson');
                    $.ajax({
                        url: '/products/' + param + '?view=json',
                        type: 'GET'
                    })
                    .done(function(data) {
                        console.log('type: ' + type);
                        console.log('loadProductJson done for ' + type + '...');
                        var productData = $.parseJSON(data);

                        console.log(productData);
                        console.log(productData.metafields);

                            // $('#build-wrapper').removeClass('is-hidden');

                            if (type == 'diamond') {
                                var productId, productTitle, productPrice, productImage, productVideo, productStyle, productSize, productUrl, collectionUrl, certLink;

                                productId = productData.product.data.variants[0].id;
                                productTitle = productData.product.data.title;
                                productPrice = productData.product.data.price;
                                productImage = productData.product.data.featured_image;
                                productVideo = productData.metafields.video_file;
                                productStyle = null;
                                productSize = null;
                                certLink = productData.certificate.link;

                                // diamondPrice = productPrice;

                                // console.log(productImage);

                                Cookies.set('diamond_id', productId);
                                Cookies.set('diamond_handle', diamond_handle);
                                Cookies.set('diamond_title', productTitle);
                                Cookies.set('diamond_price', productPrice);
                                Cookies.set('diamond_image', productImage);
                                Cookies.set('diamond_video', productVideo);

                                // if (ring_style == undefined) {
                                //   ring_style = Cookies.get('ring_variant');
                                // }

                                collectionUrl = '/collections/all-lab-grown-diamonds/?diamond=' + diamond_handle + '&ring=' + ring_handle + '&style=' + ring_style;
                                productUrl = '/collections/all-lab-grown-diamonds/products/' + diamond_handle + '?ring=' + ring_handle + '&style=' + ring_style;

                                buildStep(
                                    'diamond',
                                    productId,
                                    productTitle,
                                    productPrice,
                                    productImage,
                                    productAlt,
                                    productVideo,
                                    productStyle,
                                    productSize,
                                    productUrl,
                                    collectionUrl,
                                    certLink
                                    );

                            } else if (type == 'ring') {

                                // console.log('styleParam: ' + styleParam);
                                var productId, productTitle, productPrice, productImage, productAlt, productVideo, productStyle, productSize, productUrl, collectionUrl, certLink;

                                // console.log('styleParam:' + styleParam);
                                // console.log('style Cookie:' + Cookies.get('ring_variant'));

                                var variantStyle = (styleParam != undefined) ? styleParam : Cookies.get('ring_variant');

                                // console.log('variantStyle ' + variantStyle);
                                // console.log(productData.product.data);

                                $.each(productData.product.data.variants, function(key, value) {
                                    if (value.id == variantStyle) {
                                        console.log('match!');
                                        productId = variantStyle;
                                        productTitle = productData.product.data.title;
                                        productPrice = value.price;

                                        if (value.featured_image != null) {
                                            productImage = value.featured_image.src;
                                            productAlt = value.featured_image.alt;
                                        } else {
                                            productImage = productData.product.data.featured_image;
                                            productAlt = productData.product.data.title;
                                        }

                                        productVideo = productData.metafields.video_file;
                                        productStyle = value.option1;
                                        productSize = value.option2;
                                        certLink = null;
                                        return false;
                                    }
                                });

                                // ringPrice = productPrice;

                                // console.log(productImage);
                                Cookies.set('ring_handle', ring_handle);
                                Cookies.set('ring_variant', variantStyle);
                                Cookies.set('ring_title', productTitle);
                                Cookies.set('ring_price', productPrice);
                                Cookies.set('ring_image', productImage);
                                Cookies.set('ring_alt', productAlt);
                                Cookies.set('ring_video', productVideo);
                                Cookies.set('ring_style', productStyle);
                                Cookies.set('ring_size', productSize);

                                collectionUrl = '/collections/engagement-ring-settings/?ring=' + ring_handle + '&variant=' + ring_style + '&diamond=' + diamond_handle;
                                productUrl = '/collections/engagement-ring-settings/products/' + ring_handle + '?variant=' + ring_style + '&diamond=' + diamond_handle;

                                buildStep(
                                    'ring',
                                    productId,
                                    productTitle,
                                    productPrice,
                                    productImage,
                                    productAlt,
                                    productVideo,
                                    productStyle,
                                    productSize,
                                    productUrl,
                                    collectionUrl,
                                    certLink
                                    );
                            }

                        })
.fail(function(data) {
    console.log('loadProductJson failed');
})
.always(function(data) {

});
}

function buildStep(
    type,
    productId,
    productTitle,
    productPrice,
    productImage,
    productAlt,
    productVideo,
    productStyle,
    productSize,
    productUrl,
    collectionUrl,
    certLink
    ) {
    if (productTitle && productTitle != undefined) {
        var $itemBox = $('.paired-item');
        var $bundleButton = $('#add-to-cart.bundle-button');
        var $thumbnail = $itemBox.find('a.thumbnail');
        var $title = $itemBox.find('a.title');
        var $price = $itemBox.find('.price');
        var $button = $('#build-your-ring-button');

        $('.paired-item').removeClass('is-hidden');
        $itemBox.attr('data-type', type);

        if (type == 'diamond') {
            $bundleButton.attr('data-diamond', productId)
        } else {
            $bundleButton.attr('data-ring', productId)
        }

        var $img = $('<img>');
        var fullTitle = (type == 'ring') ? productTitle + ' (' + productStyle + ' / ' + productSize + ')' : productTitle;
        var productPriceText = '' + productPrice;
        var buttonText = (type == 'ring') ? 'Change Your Ring' : 'Change Your Diamond';
        var currentPagePrice = $('span.product-price').attr('data-price');
        var selectedDiamondPrice = $('span.product-price').attr('data-price');
        var priceTrimmed = productPriceText.substring(0, productPriceText.length - 2);
        var totalPrice;
        var totalPriceFormatted;

        productPriceText = '<strong>$' + productPriceText.substring(0, productPriceText.length - 2) + '</strong>';

        $img.attr('src', productImage);
        $img.attr('alt', fullTitle);
        $thumbnail.append($img);

        $title.attr('href', productUrl);
        $title.append(fullTitle);

        $price.append(numberWithCommas(productPriceText));

        $button.find('span').html(buttonText);

        console.log(productUrl);

        console.log(parseInt(priceTrimmed, 10));
        console.log(parseInt(currentPagePrice, 10));

        if (type == 'diamond') {
            console.log('certLink: ' + certLink);
            if (certLink != null && certLink != undefined) {
                $('span.product-vendor a').attr('href', certLink);
                $('span.product-vendor').removeClass('is-hidden');
            }

                            // populate pricing data
                            $('.diamond-price span.item-price').html('$' + numberWithCommas(priceTrimmed)).attr("data-price", priceTrimmed);
                            $('.ring-price span.item-price').html('$' + numberWithCommas(parseInt(currentPagePrice, 10))).attr("data-pice", currentPagePrice);
                            $('.diamond-price').removeClass('is-hidden');
                            $('.ring-price').removeClass('is-hidden');

                        } else {

                            // populate pricing data
                            $('.diamond-price span.item-price').html('$' + numberWithCommas(parseInt(currentPagePrice, 10))).attr("data-price", currentPagePrice);
                            $('.ring-price span.item-price').html('$' + numberWithCommas(priceTrimmed)).attr("data-price", priceTrimmed);
                            $('.diamond-price').removeClass('is-hidden');
                            $('.ring-price').removeClass('is-hidden');
                        }

                        totalPrice = parseInt(priceTrimmed, 10) + parseInt(currentPagePrice, 10);
                        totalPriceFormatted = '$' + numberWithCommas(totalPrice);

                        $('span.product-price').html(totalPriceFormatted);

                        $('.affirm-as-low-as').attr('data-amount', totalPrice * 100);
                        affirm.ui.refresh();
                    }
                }
                $('a.remove').on('click', function(e) {
                    e.preventDefault();
                    var $bundleButton = $('#add-to-cart.bundle-button');
                    // var diamondChoose = $('.diamond-price span.item-price').attr('data-link');
                    // var ringChoose = $('.ring-price span.item-price').attr('data-link');

                    if ($('.paired-item').attr('data-type') == 'ring') {
                        console.log('remove ring');
                        Cookies.remove('ring_handle');
                        Cookies.remove('ring_variant');
                        Cookies.remove('ring_title');
                        Cookies.remove('ring_price');
                        Cookies.remove('ring_image');
                        Cookies.remove('ring_alt');
                        Cookies.remove('ring_video');
                        Cookies.remove('ring_style');
                        Cookies.remove('ring_size');
                        $bundleButton.attr('data-ring', '');
                        $('.build-button-text').html('Choose Your Ring');
                        $('.ring-price').addClass('is-hidden');
                    } else if ($('.paired-item').attr('data-type') == 'diamond') {
                        console.log('remove diamond');
                        Cookies.remove('diamond_id');
                        Cookies.remove('diamond_handle');
                        Cookies.remove('diamond_title');
                        Cookies.remove('diamond_price');
                        Cookies.remove('diamond_image');
                        Cookies.remove('diamond_video');
                        $bundleButton.attr('data-diamond', '');
                        $('.build-button-text').html('Choose Your Diamond');
                        $('.diamond-price').addClass('is-hidden');
                    }

                    $('.paired-item').addClass('is-hidden');

                    //$('.product-price').
                    var newPrice = $('.product-price').attr('data-price');
                    $('.product-price').html('$' + numberWithCommas(newPrice));
                    console.log('newPrice: ' + numberWithCommas(newPrice));
                    $('.affirm-as-low-as').attr('data-amount', newPrice * 100);
                    affirm.ui.refresh();

                    callForButtonUpdate();
                });

                function numberWithCommas(x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }

                function callForButtonUpdate() {

                    // this._updateBuildButton();
                    var buildButton = $('#build-your-ring-button');

                    if (buildButton.hasClass('is-ring')) {
                        var ring_handle = (Cookies.get('ring_handle') != undefined) ? Cookies.get('ring_handle') : 'null';
                        var ring_style = (Cookies.get('ring_variant') != undefined) ? Cookies.get('ring_variant') : 'null';
                        var url = '/collections/all-lab-grown-diamonds?ring=' + ring_handle + '&style=' + ring_style;

                        buildButton.attr('href', url);

                    } else if (buildButton.hasClass('is-diamond')) {
                        var diamond_handle = (Cookies.get('diamond_handle') != undefined) ? Cookies.get('diamond_handle') : "null";
                        var url = '/collections/engagement-ring-settings?diamond=' + diamond_handle;

                        buildButton.attr('href', url);

                    }

                }

                this._updateBuildButton();

                var productIsDisabled = false;

                if ($product.find('.product--section').data('hide-variants') && $product.find('.product--section').data('product-available')) {
                    this._advancedOptionsSelector($product, JSON.parse($product.find('.product-json')[0].innerHTML));
                } else if ($product.find('.product--section').data('hide-variants') && !$product.find('.product--section').data('product-available')) {
                    productIsDisabled = true;
                }

                // Product gallery

                var _this = {};
                $product.data('po', _this);

                // console.log('product gallery...');
                // console.log(window.CuberProductImageIndex);

                _this.$productGallery = $product.find('.box__product-gallery');
                _this.$productGalleryItem = $product.find('.box__product-gallery .gallery-item');

                _this.$productGallery.append('<div class="gallery-index out-with-you"><span class="current">' + (window.CuberProductImageIndex != undefined ? (window.CuberProductImageIndex + 1) : 1) + '</span> / <span class="total">' + _this.$productGalleryItem.length + '</span></div>');
                _this.$productGalleryIndex = _this.$productGallery.find('.gallery-index .current');

                _this.$productCarousel = _this.$productGallery.children('.site-box-content');

                window.CUBER.Main._mountScrollMovers({
                    'parent': _this.$productGallery,
                    'items': $('.gallery-index, .site-sharing, .product-zoom')
                });

                window.CUBER.Main._mobileSharingInit();

                if (_this.$productGallery.hasClass('scroll')) {

                    // init scrollable gallery

                    $(window).on('scroll.product-gallery', (function() {

                        if (!_this.$productCarousel.hasClass('flickity-enabled')) {

                            _this.$productGalleryItem.each((function(key, elm) {

                                if ($(window).scrollTop() + $(window).height() > $(elm).offset().top + $(window).height() / 2 && !$(elm).hasClass('current')) {

                                    $(elm).addClass('current');
                                    _this.$productGalleryIndex.html($(elm).index() + 1);

                                } else if ($(window).scrollTop() + $(window).height() < $(elm).offset().top + $(window).height() / 2 && $(elm).hasClass('current')) {

                                    $(elm).removeClass('current');
                                    _this.$productGalleryIndex.html($(elm).index());

                                }

                            }).bind(_this));

                        }

                    }).bind(_this)).trigger('scroll.product-gallery');

                }

                // init sliding gallery (always, because it turns into this at responsive)


                // console.log('carousel init...');
                // console.log(window.CuberProductImageIndex);

                _this.$productCarousel.flickity({
                    cellSelector: '.gallery-item:not(.remove-from-flick)',
                    initialIndex: window.CuberProductImageIndex != undefined ? window.CuberProductImageIndex : 0,
                    wrapAround: true,
                    prevNextButtons: false,
                    pageDots: true,
                    watchCSS: _this.$productGallery.hasClass('scroll') ? true : false,
                    resize: true
                });

                console.log('post carousel init...');

                window.CUBER.Scroll.mount();

                _this.productFlkty = _this.$productCarousel.data('flickity');
                // console.log('hello?');
                // console.log(_this.productFlkty);
                _this.$productCarouselImgs = _this.$productCarousel.find('.gallery-item img');

                _this.$productCarousel.on('select.flickity', (function() {
                    _this.$productGalleryIndex.html((_this.productFlkty.selectedIndex + 1));
                }).bind(_this))

                if (_this.$productGallery.hasClass('slider')) {

                    _this.$productGallery.find('.gallery-index').append('<span class="icon-go go-prev">' + $.themeAssets.arrowRight + '</span><span class="icon-go go-next">' + $.themeAssets.arrowRight + '</span>');

                    _this.$productGallery.find('.go-prev').on('click', (function() {
                        _this.productFlkty.previous();
                    }).bind(_this));

                    _this.$productGallery.find('.go-next').on('click', (function() {
                        _this.productFlkty.next();
                    }).bind(_this));

                }

                // Builder

                // this._getParameterByName()

                // Product zoom

                if ($('#product-zoom-in').length > 0) {

                    $('body').append('<div id="product-zoomed-image"><img /><div id="product-zoom-out" class="product-zoom expand"><span class="zoom-out">' + $.themeAssets.iconZoom + '</span></div></div>');

                    $('#product-zoom-in').on('click', (function(e) {

                        // animation out

                        $('#section-product').find('.site-box.box__product-content').addClass('animate');
                        setTimeout(function() {
                            $('#section-product').find('.site-box.box__product-content').addClass('expand');
                            $('body').addClass('kill-overflow');
                        }, 10);

                        $('#section-product').find('.site-box.box__product-gallery').stop().animate({
                            'opacity': 0
                        }, 200);
                        $('#shopify-section-header, #shopify-section-footer, .site-sharing').stop().animate({
                            'opacity': 0
                        }, 200);

                        // object

                        var $image = $('#product-zoomed-image img');

                        console.log('type: ' + $('.gallery-item').eq(parseInt(_this.$productGalleryIndex.html()) - 1).data('type'));

                        if ($('.gallery-item').eq(parseInt(_this.$productGalleryIndex.html()) - 1).data('type') == 'video') {

                            var videoWrapper = document.createElement('div');
                            $(videoWrapper).addClass('video-wrapper');

                            var videoPlayer = document.createElement('video');
                            $(videoPlayer).addClass('video-js');

                            videoWrapper.append(videoPlayer);

                            $image.replaceWith(videoWrapper);

                            videojs(videoPlayer, {
                                autoplay: true,
                                muted: true,
                                loop: true,
                                preload: 'auto',
                                aspectRatio: '1:1'
                            });

                            videojs(videoPlayer).src({
                                type: 'video/mp4',
                                src: $('.gallery-item').eq(parseInt(_this.$productGalleryIndex.html()) - 1).data('master')
                            });


                        } else {
                            $image.attr('src', $('.gallery-item').eq(parseInt(_this.$productGalleryIndex.html()) - 1).data('master'));
                        }

                        setTimeout((function() {

                            if (typeof videoPlayer !== 'undefined') {

                                this._productZoomMount(videoWrapper, true);

                            } else {

                                if ($image[0].naturalWidth > 0) {
                                    this._productZoomMount($image, false);
                                } else {
                                    $image.on('load', (function() {
                                        this._productZoomMount($image, false);
                                    }).bind(this))
                                }

                            }

                        }).bind(this), 200);

                    }).bind(this));

                    $('#product-zoom-out').on('click', (function(e) {

                        setTimeout(function() {

                            // animation out

                            $('#section-product').find('.site-box.box__product-content').removeClass('expand');
                            $('body').removeClass('kill-overflow')
                            setTimeout(function() {
                                $('#section-product').find('.site-box.box__product-content').removeClass('animate');
                            }, 400);

                            $('#section-product').find('.site-box.box__product-gallery').stop().animate({
                                'opacity': 1
                            }, 200);
                            $('#shopify-section-header, #shopify-section-footer, .site-sharing').stop().animate({
                                'opacity': 1
                            }, 200);

                        }, 150);

                        // object

                        this._productZoomUnmount();

                    }).bind(this));

                }

                // Add to cart (& ajax)

                if (!$product.find('.product--section').hasClass('onboarding-true')) {
                    this.initProductForm($product, productIsDisabled);
                }

                if ($product.find('.qty-button').length > 0) {

                    var $productQty = $product.find('.quantity-selector');

                    if (parseInt($productQty.val()) - 1 < parseInt($productQty.attr('min'))) {
                        $product.find('.qty-minus').addClass('disabled');
                    }
                    if (parseInt($productQty.val()) + 1 > parseInt($productQty.attr('max'))) {
                        $product.find('.qty-plus').addClass('disabled');
                    }

                    $product.find('.qty-minus').on('click', (function($productQty, $productQtyPlus, e) {
                        if (!$(e.target).hasClass('disabled')) {
                            var currentQty = parseInt($productQty.val());
                            if (currentQty - 1 >= parseInt($productQty.attr('min'))) {
                                $productQty.val(currentQty - 1);
                                $productQtyPlus.removeClass('disabled');
                            }
                            if (currentQty - 1 <= parseInt($productQty.attr('min'))) {
                                $(e.target).addClass('disabled');
                            }
                        }
                        e.preventDefault();
                    }).bind(this, $productQty, $product.find('.qty-plus')));

                    $product.find('.qty-plus').on('click', (function($productQty, $productQtyMinus, e) {
                        if (!$(e.target).hasClass('disabled')) {
                            var currentQty = parseInt($productQty.val());
                            if (currentQty + 1 <= parseInt($productQty.attr('max'))) {
                                $productQty.val(currentQty + 1);
                                $productQtyMinus.removeClass('disabled');
                            }
                            if (currentQty + 1 >= parseInt($productQty.attr('max'))) {
                                $(e.target).addClass('disabled');
                            }
                        }
                        e.preventDefault();
                    }).bind(this, $productQty, $product.find('.qty-minus')));

                }

                if ($product.find('.product--add-to-cart-form').length > 0 && $product.find('.product--add-to-cart-form').data('type') == 'overlay' && !$('html').hasClass('ie9')) {

                    var $form = $product.find('.product--add-to-cart-form form'),
                    $submitText = $form.find('.add-to-cart-text'),
                    $submitButton = $form.find('button[type="submit"]'),
                    $cartCount = $('.cart-menu .count'),
                    $cartQty = $product.find('.quantity-selector');

                    if ($form.attr('action').indexOf('.js') < 0) {
                        $form.attr('action', $form.attr('action') + '.js');
                    }

                    $form.submit(function(e) {


                        e.preventDefault();


                        // console.log('submit hit');


                        // if (!$('.paired-item').hasClass('is-hidden')) {
                            if ($('.paired-item').length > 0) {

                                console.log('we have a paired item');

                                if (!$('.paired-item').hasClass('is-hidden')) {

                                    console.log('we have a paired item, so override submit action');

                                    Shopify.queue = [];

                                    Shopify.queue.push({
                                        variantId: Cookies.get('diamond_id'),
                                        quantity: 1
                                    });

                                    Shopify.queue.push({
                                        variantId: Cookies.get('ring_variant'),
                                        quantity: 1
                                    });

                                    Shopify.moveAlong = function() {
                                    // if we still have requests in the queue, process the next one.
                                    if (Shopify.queue.length) {
                                        var request = Shopify.queue.shift();
                                        Shopify.addItem(request.variantId, request.quantity, Shopify.moveAlong);
                                    }
                                    // if the queue is empty, redirect to the cart page.
                                    else {
                                        // console.log('finished!');
                                        document.location.href = '/cart';
                                    }
                                };
                                Shopify.moveAlong();

                            } else {

                                var oldText = $submitText.html();
                                $submitText.html('<span class="preloader"><span>.</span><span>.</span><span>.</span></span>');
                                $submitButton.css('pointer-events', 'none');

                                var itemName = $product.find('.product-title').text(),
                                itemPrice = $product.find('.product-price').attr('data-price'),
                                itemCurrency = $('meta[itemprop="priceCurrency"]').attr('content'),
                                itemId = $product.find('.product--section').data('id');

                                $.ajax({

                                    type: $form.prop('method'),
                                    url: $form.prop('action'),
                                    data: $form.serialize(),
                                    dataType: 'json',
                                    success: function(data) {

                                        setTimeout(function() {
                                            $submitText.html(oldText);
                                            $submitButton.css('pointer-events', 'all');
                                        }, 500);

                                        $.ajax({
                                            url: '/cart',
                                            success: function(data) {

                                                if (typeof fbq !== 'undefined') {
                                                    fbq('track', 'AddToCart', {
                                                        content_name: itemName,
                                                        content_ids: [itemId],
                                                        content_type: 'product_group',
                                                        value: itemPrice,
                                                        currency: itemCurrency
                                                    });
                                                }

                                                $('#site-cart .cart-items').html($(data).find('#site-cart .cart-items .cart-item'));
                                                $('#CartTotal').html($(data).find('#CartTotal').html());
                                                $('#CartDetails').html($(data).find('#CartDetails').html());

                                                window.sidebarCartAjaxFunctions();

                                                setTimeout(function() {
                                                    $submitText.html(oldText);
                                                    $submitButton.css('pointer-events', 'all');
                                                }, 500);

                                                if ($cartQty.length > 0) {

                                                    var qty = parseInt($cartQty.val());
                                                    if (qty == 1) {
                                                        $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/g, qty));
                                                    } else {
                                                        $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-plural').replace(/{{ count }}|count|{{count}}/g, qty));
                                                    }

                                                    $cartCount.text(parseInt($cartCount.text()) + parseInt($cartQty.val()));

                                                } else {

                                                    $cartCount.text(parseInt($cartCount.text()) + 1);
                                                    $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/, 1));

                                                }

                                                $('.site-cart-handle a').trigger('click');

                                            }
                                        });

                                    },

                                    error: function(data) {

                                        alert(data.responseJSON.description);

                                        setTimeout(function() {
                                            $submitText.html(oldText);
                                            $submitButton.css('pointer-events', 'all');
                                        }, 100);

                                    }
                                });


}

} else {

    var oldText = $submitText.html();
    $submitText.html('<span class="preloader"><span>.</span><span>.</span><span>.</span></span>');
    $submitButton.css('pointer-events', 'none');

    var itemName = $product.find('.product-title').text(),
    itemPrice = $product.find('.product-price').attr('data-price'),
    itemCurrency = $('meta[itemprop="priceCurrency"]').attr('content'),
    itemId = $product.find('.product--section').data('id');

    $.ajax({

        type: $form.prop('method'),
        url: $form.prop('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(data) {

            setTimeout(function() {
                $submitText.html(oldText);
                $submitButton.css('pointer-events', 'all');
            }, 500);

            $.ajax({
                url: '/cart',
                success: function(data) {

                    if (typeof fbq !== 'undefined') {
                        fbq('track', 'AddToCart', {
                            content_name: itemName,
                            content_ids: [itemId],
                            content_type: 'product_group',
                            value: itemPrice,
                            currency: itemCurrency
                        });
                    }

                    $('#site-cart .cart-items').html($(data).find('#site-cart .cart-items .cart-item'));
                    $('#CartTotal').html($(data).find('#CartTotal').html());
                    $('#CartDetails').html($(data).find('#CartDetails').html());

                    window.sidebarCartAjaxFunctions();

                    setTimeout(function() {
                        $submitText.html(oldText);
                        $submitButton.css('pointer-events', 'all');
                    }, 500);

                    if ($cartQty.length > 0) {

                        var qty = parseInt($cartQty.val());
                        if (qty == 1) {
                            $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/g, qty));
                        } else {
                            $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-plural').replace(/{{ count }}|count|{{count}}/g, qty));
                        }

                        $cartCount.text(parseInt($cartCount.text()) + parseInt($cartQty.val()));

                    } else {

                        $cartCount.text(parseInt($cartCount.text()) + 1);
                        $('#site-cart .subtitle').html($('#site-cart .subtitle').data('added-singular').replace(/{{ count }}|count|{{count}}/, 1));

                    }

                    $('.site-cart-handle a').trigger('click');

                }
            });

        },

        error: function(data) {

            alert(data.responseJSON.description);

            setTimeout(function() {
                $submitText.html(oldText);
                $submitButton.css('pointer-events', 'all');
            }, 100);

        }
    });


}

});

}

},

_getParameterByName: function(name) {

    var url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));

},

_productZoomMount: function($image, is_video) {

    if (is_video) {

        $('#product-zoomed-image').css('display', 'block');
        $($image).css({
            'left': 0,
            'top': 0
        });

        $(window).on('mousemove.product-zoom', function(e) {
            e.preventDefault();
            window.clientX = e.clientX;
            window.clientY = e.clientY;
            ``
            var x = e.clientX * ($(window).width() - $($image).width()) / $(window).width();
            var y = e.clientY * ($(window).height() - $($image).height()) / $(window).height();

            $($image).css({
                'left': x,
                'top': y
            });
        });

        $(window).on('resize.product-zoom', function() {

            var rf = $(window).width() > 768 ? 1 : 2;

            $($image).css('width', $(window).width() * rf);
            $($image).css('height', $(window).width() * rf);

            var x = window.clientX * ($(window).width() - $($image).width()) / $(window).width();
            var y = window.clientY * ($(window).height() - $($image).height()) / $(window).height();

                        //$image.css({'left': x, 'top': y});

                    }).trigger('resize');

        $($image).css('opacity', 1);

    } else {

        $('#product-zoomed-image').css('display', 'block');
        $($image).css({
            'left': 0,
            'top': 0
        });

        $(window).on('mousemove.product-zoom', function(e) {
            e.preventDefault();
            window.clientX = e.clientX;
            window.clientY = e.clientY;
            ``
            var x = e.clientX * ($(window).width() - $image.width()) / $(window).width();
            var y = e.clientY * ($(window).height() - $image.height()) / $(window).height();

            $($image).css({
                'left': x,
                'top': y
            });

        });

        if ($image[0].naturalWidth <= $image[0].naturalHeight) {
            $image.addClass('portrait');
        } else {
            $image.addClass('landscape');
        }
        $image.data('ratio', $image[0].naturalWidth / $image[0].naturalHeight);

        $(window).on('resize.product-zoom', function() {

            var rf = $(window).width() > 768 ? 1 : 2;

            if ($image.hasClass('portrait')) {
                $image.css('width', $(window).width() * rf);
                $image.css('height', $(window).width() * rf / $image.data('ratio'));
            } else {

                $image.css('height', $(window).height() * rf);
                $image.css('width', $(window).height() * rf * $image.data('ratio'));

                if ($image.width() < $(window).width()) {
                    $image.css('width', $(window).width() * rf);
                    $image.css('height', $(window).width() * rf / $image.data('ratio'));
                }

            }

            var x = window.clientX * ($(window).width() - $image.width()) / $(window).width();
            var y = window.clientY * ($(window).height() - $image.height()) / $(window).height();

                        //$image.css({'left': x, 'top': y});

                    }).trigger('resize');

        $image.css('opacity', 1);

    }

},

_productZoomUnmount: function() {

    $('#product-zoomed-image img').css('opacity', '0');

    setTimeout(function() {
        $(window).off('resize.product-zoom');
        $(window).off('mousemove.product-zoom');
        $('#product-zoomed-image img').attr('src', '');
        $('#product-zoomed-image').css('display', 'none');
    }, 300);

},

initProductForm: function($product, pdisabled) {

    var firstVariantEver = true;

    var productSingleObject = JSON.parse($product.find('.product-json')[0].innerHTML),
    productVariants = productSingleObject.variants;

    $product.find('form select.product-variants').on('change', (function(e) {
        this._initProductVariantChange(false, productSingleObject, $product);
    }).bind(this));

    this._initProductVariantChange(true, productSingleObject, $product);

    if ($product.find('.product--add-to-cart-form').hasClass('style--classic')) {

        $product.find('select.product-variants:not(.styled)').each(function() {

            $(this).styledSelect({
                coverClass: 'regular-select-cover',
                innerClass: 'regular-select-inner'
            }).addClass('styled');

            $(this).parent().append($.themeAssets.arrowDown);

            $(this).on('focusin', function() {
                $(this).parent().addClass('focus');
            }).on('focusout', function() {
                $(this).parent().removeClass('focus');
            });

        });

        $product.find('.product-variant').removeClass('hidden');

        if (pdisabled) {
            $product.find('.product-variant').css('display', 'none');
        }

    } else {

        var i = 1;

        $product.find('.product-variant').each(function(j) {

            var color = window.returnColorVariant($(this).find('label').text()) ? true : false,
            varDOM = '<ul class="color-' + color + '" data-option="option' + (j + 1) + '">';

            $(this).find('.product-variants option').each(function() {
                varDOM += '<li' + ($(this).is(':selected') ? ' class="active"' : '') + ' tabindex="0" data-text="' + $(this).val() + '"><span' + (color ? ' style="background-color: ' + $(this).val().split(' ').pop() + '"' : '') + '>' + $(this).val() + '</span></li>';
            });

            varDOM += '</ul>';
            $(this).find('select').hide();
            $(this).append(varDOM);

            $(this).removeClass('hidden');

            $(this).find('ul li').on('click', function() {
                $(this).parent().parent().find('select').val($(this).find('span').text()).change();
            });

            $(this).find('ul li').on('keyup', function(e) {
                if (e.keyCode == 13) {
                    $(this).parent().parent().find('select').val($(this).find('span').text()).change();
                }
            });

        });

        if (pdisabled) {
            $product.find('.product-variant').find('li').addClass('disabled');
        }

    }

    if (productSingleObject.variants.length == 1 && productSingleObject.variants[0].title.indexOf('Default') >= 0) {
        $product.find('.product-variant').hide();
    }

},

_getProductVariant: function($product) {

                /* ----
                	Get current variant
                	---- */

                    var optionArray = [];

                    $product.find('form select.product-variants').each(function() {
                        optionArray.push($(this).find(':selected').val())
                    });

                    return optionArray;

                },

                _initProductVariantChange: function(firstTime, productSingleObject, $product) {

                    var variant = null,
                    options = this._getProductVariant($product);

                    productSingleObject.variants.forEach(function(el) {

                        if ($(el)[0].options.equals(options)) {
                            variant = $(el)[0];
                        }

                    });

                    this._productSelectCallback(variant, $product, productSingleObject);
                    if (!firstTime && $product.find('#productSelect option').length > 1 && !$('body').hasClass('template-index')) {
                        this._updateProductVariantState(variant);
                        console.log('_productSelectCallback called');
                    }

                },

                _updateProductVariantState: function(variant) {

                    if (!history.pushState || !variant) {
                        return;
                    }

                    console.log('_updateProductVariantState called');

                    var $bundleButton = $('#add-to-cart.bundle-button');
                    $bundleButton.attr('data-ring', variant.id);

                    var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?variant=' + variant.id;
                    window.history.replaceState({
                        path: newurl
                    }, '', newurl);

                // if this product is a semimount or diamond then udpate button link

                //////////////
                // SHIPPING //
                //////////////

                var shipData = $('#productSelect option:selected').attr('data-inventory');
                var shipDate = $('#shippingNotice').attr('data-shipping');
                var orderDate = $('#shippingNotice').attr('data-preorder');

                var shippingText = (shipData > 0) ? shipDate : orderDate;

                // console.log(variant);
                //
                // console.log('shipData: ' + shipData);
                //
                // console.log(shippingText);

                $('#shippingNotice').html(shippingText);


                var buildButton = $('#build-your-ring-button');

                ///////////////////
                // UPDATE BUTTON //
                ///////////////////

                if (buildButton.length > 0) {

                    if (buildButton.hasClass('is-ring')) {
                        var link_collection = buildButton.attr('data-collection');
                        var link_handle = '?ring=' + buildButton.attr('data-handle');
                        var link_variant = '&style=' + variant.id;
                        buildButton.attr('href', link_collection + link_handle + link_variant);

                        Cookies.set('ring_variant', variant.id);
                        console.log('update variant cookie: ' + Cookies.get('ring_variant'));
                        this._updateBuildButton();
                    }

                }


            },

            _updateBuildButton: function() {

                var buildButton = $('#build-your-ring-button');

                if (buildButton.hasClass('is-ring')) {
                    var handle = (Cookies.get('diamond_handle') != undefined) ? Cookies.get('diamond_handle') : "null";
                    var variant = Cookies.get('diamond_variant');
                    var vid = Cookies.get('diamond_video');
                    var buttonUrl = buildButton.attr('href');
                    var urlParameter = '&diamond=' + handle;
                    var newButtonUrl = buttonUrl + urlParameter;
                    buildButton.attr('href', newButtonUrl);
                    console.log('newButtonUrl: ' + newButtonUrl);
                } else if (buildButton.hasClass('is-diamond')) {
                    var handle = (Cookies.get('ring_handle') != undefined) ? Cookies.get('ring_handle') : "null";
                    var variant = (Cookies.get('ring_variant') != undefined) ? Cookies.get('ring_variant') : "null";
                    var vid = Cookies.get('ring_video');
                    var buttonUrl = buildButton.attr('href');
                    var urlParameter = '&ring=' + handle + '&style=' + variant;
                    var newButtonUrl = buttonUrl + urlParameter;
                    buildButton.attr('href', newButtonUrl);
                    console.log('newButtonUrl: ' + newButtonUrl);
                }

            },

            _productSelectCallback: function(variant, $product, productSingleObject) {

                var _po = $product.data('po');

                var $addToCart = $product.find('.product--add-to-cart-form'),
                $addToCartButton = $addToCart.find('button[type="submit"]'),
                $productPrice = $product.find('.product-price'),
                $comparePrice = $product.find('.compare-price'),
                $quantityElements = $product.find('.quantity-selector, label + .js-qty'),
                $quantityElementsHolder = $product.find('.quantity-selector-holder'),
                $addToCartText = $product.find('.add-to-cart-text'),
                $productGallery = $product.find('.box__product-gallery');

                if (variant) {

                    // console.log('line 2772');
                    // console.log(variant.featured_image.alt);

                    // Set cart value id

                    $product.find('#productSelect').find('option[value="' + variant.id + '"]').prop('selected', true);

                    // Swipe to variant slide

                    if (variant.featured_image != null) {

                        var variantImg = $productGallery.find('.gallery-item[data-variant-img="' + variant.featured_image.id + '"]');
                        // console.log(variantImg.data());
                        // console.log(variantImg.data('index'));

                        if ($productGallery.hasClass('scroll') && variantImg.length > 0 && $(window).width() > 1024) {

                            if ($(window).scrollTop() > 0) {
                                $('html, body').stop().animate({
                                    'scrollTop': $productGallery.offset().top
                                }, 150, function() {
                                    $(window).off('scroll.to-junk');
                                })
                            }

                            variantImg.prependTo($product.find('.box__product-gallery .site-box-content'))

                        } else if (_po != undefined && variantImg.length > 0) {
                            // console.log('swipe to variant');
                            // console.log(variant.featured_image.alt);
                            $('.box__product-gallery').find('img').each(function(index) {
                                // console.log('loop:' + $(this).attr('alt'));
                                if ($(this).attr('alt') == variant.featured_image.alt) {
                                    // console.log('match for: ' + variant.featured_image.alt + ': slide=' + index);
                                    // window.CuberProductImageIndex = index;
                                    _po.productFlkty.select(index);

                                }

                            });

                        }

                    }

                    // Disable other variants

                    if ($addToCart.hasClass('style--minimal')) {
                        $product.find('.product-variant ul').each(function() {
                            var option = variant[$(this).data('option')];
                            $(this).find('li').each(function() {
                                $(this).removeClass('active');
                                if (option == $(this).data('text')) {
                                    $(this).addClass('active');
                                }
                            });
                        })
                    }

                    // Edit cart buttons based on stock

                    var $variantQuantity = $product.find('.variant-quantity');

                    if (variant.available) {

                        $quantityElements.prop('max', 999);
                        $addToCartButton.removeClass('disabled').prop('disabled', false);
                        $addToCartText.text($('body').hasClass('template-product-pre-order') ? $addToCartText.data('text') : window.product_words_add_to_cart_button);
                        $variantQuantity.text($('body').hasClass('template-product-pre-order') ? $variantQuantity.data('text') : '');
                        $quantityElements.show();
                        if ($quantityElementsHolder != null) {
                            $quantityElementsHolder.show();
                        }

                    } else {

                        $variantQuantity.text(window.product_words_no_products);
                        $addToCartButton.addClass('disabled').prop('disabled', true);
                        $addToCartText.text(window.product_words_sold_out_variant);
                        $quantityElements.hide();
                        if ($quantityElementsHolder != null) {
                            $quantityElementsHolder.hide();
                        }

                    }

                    // Update price

                    $productPrice.html(theme.Currency.formatMoney(variant.price, window.shop_money_format));
                    $productPrice.attr('data-price', variant.price / 100);
                    if (variant.compare_at_price > variant.price) {
                        $comparePrice.html(theme.Currency.formatMoney(variant.compare_at_price, window.shop_money_format)).show();
                    } else {
                        $comparePrice.hide();
                    }

                    // Update sku

                    if ($product.find('.variant-sku').length > 0) {
                        if (variant) {
                            $product.find('.variant-sku').text(variant.sku);
                        } else {
                            $product.find('.variant-sku').empty();
                        }
                    }

                } else {

                    // Disable variant completely

                    $addToCartButton.addClass('disabled').prop('disabled', true);
                    $addToCartText.text(window.product_words_unavailable_variant);
                    $quantityElements.hide();
                    if ($quantityElementsHolder != null) {
                        $quantityElementsHolder.hide();
                    }

                }

                if ($product.find('.qty-button').length > 0) {

                    var $productQty = $product.find('.quantity-selector');
                    $product.find('.qty-minus').removeClass('disabled');
                    $product.find('.qty-plus').removeClass('disabled');

                    if (parseInt($productQty.val()) - 1 < parseInt($productQty.attr('min'))) {
                        $product.find('.qty-minus').addClass('disabled');
                    }
                    if (parseInt($productQty.val()) + 1 > parseInt($productQty.attr('max'))) {
                        $product.find('.qty-plus').addClass('disabled');
                    }

                }

            },

            _advancedOptionsSelector: function($product, productJson) {
                var om = {};
                $product.data('om', om);
                var forceMutation = false;
                var $addToCartForm = $product.find('.product--add-to-cart-form');
                if (window.MutationObserver && $addToCartForm.length) {
                    if (typeof observer === 'object' && typeof observer.disconnect === 'function') {
                        observer.disconnect();
                    }
                    var config = {
                        childList: true,
                        subtree: true
                    };
                    var observer = new MutationObserver(function() {
                        Shopify.linkOptionSelectors(productJson, $product);
                        observer.disconnect();
                    });
                    if (forceMutation) {
                        Shopify.linkOptionSelectors(productJson, $product);
                    }
                    observer.observe($addToCartForm[0], config);
                }
            },

            unmount: function($product) {
                var po = $product.data('po');
                $(window).off('scroll.product-gallery');
                $(window).off('resize.position-product-zoom');
                po.$productCarousel.off('scroll.flickity');
                po.$productCarousel.off('select.flickity');
                po.productFlkty.destroy();
                $('#product-zoom').off('click');
            }

        },

        Main: {

            $searchForm: null,
            $searchResults: null,
            $searchPreloader: null,
            $searchPagination: null,

            $body: null,
            $siteHeader: null,
            $siteFooter: null,

            $siteBoxes: null,

            _mountScrollMovers: function(props) {

                var $parent = props['parent'],
                parentOutOfFocus = false;

                setTimeout(function() {
                    props['items'].removeClass('out-with-you');
                }, 1000);
                props['items'].addClass('animate-owy');

                $(window).on('scroll.scroll-movers', (function() {

                    /*if ( ! parentOutOfFocus && $(window).scrollTop() + $(window).height() < $parent.offset().top ) {

                    	// before element
                    	props['items'].addClass('out-with-you');
                    	parentOutOfFocus = true;

                    } else */
                    if (!parentOutOfFocus && $(window).scrollTop() + $(window).height() > $parent.offset().top + $parent.height()) {

                        // after element
                        props['items'].addClass('out-with-you');
                        parentOutOfFocus = true;

                    } else if (parentOutOfFocus && $(window).scrollTop() + $(window).height() <= $parent.offset().top + $parent.height()) {

                        // within element
                        parentOutOfFocus = false;
                        props['items'].removeClass('out-with-you');

                    }

                }).bind(this));

            },

            _mobileSharingInit: function() {

                $('.touchevents .site-sharing .icon').on('touchstart', function(e) {

                    e.preventDefault();
                    var $parent = $(this).parent();

                    if ($parent.hasClass('hover')) {
                        $parent.removeClass('hover');
                    } else {
                        $parent.addClass('hover');
                    }

                });

                $('.touchevents .site-sharing a').on('touchstart', function(e) {
                    $(this).parent().removeClass('hover');
                });

            },

            _resizeEverything: function() {

                this.$body.css('paddingTop', this.$siteHeader.outerHeight(true));

                if (this.$body.hasClass('template-index')) {

                    if ($('#fix-me-header').css('display') == 'block') {

                        if ($('.mount-slideshow').children('div').length > 0) {

                            $('.shopify-section.mount-slideshow:first-child').css('marginTop', -this.$siteHeader.outerHeight());
                            $('.fix-me-with-margin').css('marginTop', this.$siteHeader.outerHeight());

                        } else {

                            $('.index-section:first-child .fix-me-with-margin').css('marginTop', -this.$siteHeader.outerHeight());
                            $('.index-section:first-child .fix-me-with-height').css('height', $(window).height() - this.$siteHeader.outerHeight());

                        }

                    } else {

                        if ($('.mount-slideshow').children('div').length > 0) {

                            $('.shopify-section.mount-slideshow:first-child').css('marginTop', '0');
                            $('.slideshow-item:first-child .site-box-content').css('marginTop', '0');

                        }

                    }

                } else {

                    if ($('#fix-me-header').css('display') == 'block') {

                        $('.fix-me-with-margin').each((function(key, elm) {
                            if ($(elm).outerHeight() < $(window).height()) {
                                $(elm).css('marginTop', -this.$siteHeader.outerHeight())
                            } else {
                                $(elm).css('marginTop', '0')
                            }
                        }).bind(this));

                        $('.fix-me-with-height-hard').css({
                            'height': $(window).height() - this.$siteHeader.outerHeight(),
                            'min-height': $(window).height() - this.$siteHeader.outerHeight()
                        });
                        $('.fix-me-with-height').css({
                            'min-height': $(window).height() - this.$siteHeader.outerHeight()
                        });

                    } else {

                        $('.fix-me-with-margin').css('marginTop', '0');
                        $('.fix-me-with-height, .fix-me-with-height-hard').attr('style', '');

                    }

                }

                if ($('.mount-product').length > 0) {

                    $('.mount-product').each((function(key, elm) {

                        if (window.CuberFixGalleryItem) {
                            $(elm).find('.gallery-item').css('height', ($(window).height() - this.$siteHeader.outerHeight()) * 0.8);
                        }

                        $(elm).find('.product--section').removeClass('sticky-because');
                        if ($(elm).find('.site-box.box__product-gallery').css('content') != '"fix-me-also"' && $(elm).find('.site-box.box__product-gallery').height() < $(elm).find('.site-box.box__product-content').height()) {
                            $(elm).find('.product--section').addClass('sticky-because');
                        }

                    }).bind(this));

                    /*if ( $('.site-box.box__product-gallery').css('content') == '"fix-me-also"' ) {
                    	$('.site-box.box__product-gallery').css('height', $(window).height() - this.$siteHeader.outerHeight());
                    }*/

                }

                $('.site-header.desktop-view--classic .submenu').css('top', $('.site-header').outerHeight());

                if ($('#site-menu-handle').css('opacity') == '1') {
                    $('.site-nav.style--sidebar a, #site-menu-handle').attr('tabIndex', 0);
                } else {
                    $('.site-nav.style--sidebar a, #site-menu-handle').attr('tabIndex', -1);
                }

            },

            _animateEverything: function(firstAnimation) {

                var i = 0;

                this.$siteBoxes.each(function() {

                    var vp = 0;

                    if (!$(this).hasClass('active') && $(window).scrollTop() + $(window).height() > $(this).offset().top + vp) {

                        i++;

                        setTimeout((function() {
                            $(this).addClass('active');
                        }).bind(this), (100 * i));

                    }

                });

                if (!this.$siteFooter.hasClass('active') && $(window).scrollTop() + $(window).height() > this.$siteFooter.offset().top + 150) {

                    this.$siteFooter.addClass('active');
                    this.$siteFooter.find('.site-box').addClass('active');

                }

            },

            _mountCustomFooter: function($footer) {

                var txtColor = $footer.data('color_txt'),
                bgColor = $footer.data('color_bg'),
                blockId = '#' + $footer.attr('id');

                var style = '<style type="text/css">' + blockId + ' .site-box-background.with-image:after { background-color: ' + bgColor + '; } ' + blockId + ' .site-box-content { color: ' + txtColor + '; } ' + blockId + ' .site-box-content .title:after { background: ' + txtColor + '; } ' + blockId + ' .site-box-content .button { background: ' + txtColor + ' !important; color: ' + window.colorLightness(txtColor) + ' !important; } ' + blockId + ' .site-box-content .button svg * { fill: ' + window.colorLightness(txtColor) + ' !important; }</style>';

                $footer.append(style);

            },

            mount: function() {

                // Required before animation mount

                $('.rte').fitVids();

                if ($('.mount-gallery').length > 0) {
                    this._mountCustomGalleries();
                }

                if ($('.box__collection-footer').length > 0) {
                    $('.box__collection-footer').each((function(key, elm) {
                        this._mountCustomFooter($(elm));
                    }).bind(this));
                }

                // ..continue

                this.$siteBoxes = $('.site-box:not(.footer-box)');
                this.$siteFooter = $('.site-footer');

                if (!$('body').hasClass('ie9')) {

                    this._animateEverything(true);
                    $(window).on('scroll.fade-animation', (function(e) {
                        this._animateEverything(false);
                    }).bind(this));

                }

                if (window.show_preloader == "true") {
                    $('.lazyload').each(function() {
                        if ($(this).parent().hasClass('site-box-background') || $(this).parent().hasClass('box--product-image')) {
                            $(this).parent().addClass('show-lazy-preloader');
                            $(this).on('lazyloaded', function() {
                                $(this).parent().removeClass('show-lazy-preloader');
                            });
                        }
                    });
                }


                $('html.no-touchevents a[href]').each(function() {

                    $(this).on('click', function(e) {

                        console.log('clicked');

                        if ((e.ctrlKey || e.metaKey) || $(this).attr('target') == '_blank' || $(this).parents('.filter-menu').length || $(this).hasClass('cancel_animate') || $(this).hasClass('video-lightbox') || $(this).hasClass('lightbox') || $(this).hasClass('block-fade') || $(this).attr('href') == '' || $(this).attr('href').indexOf('#') >= 0) {
                            console.log('inside here?');
                        } else {
                            console.log('then why here?');
                            $(this).off('click');

                            $('body').fadeOut(200);
                            setTimeout((function() {
                                document.location.href = $(this).attr('href');
                            }).bind(this), 200);
                            e.preventDefault();
                        }

                    });

                });

                this.$body = $('body');
                this.$siteHeader = $('#site-header');

                $(window).on('resize', window.debounce((function() {
                    this._resizeEverything();
                }).bind(this), 300));
                this._resizeEverything();

                setTimeout((function() {
                    this._resizeEverything();
                }).bind(this), 1000);

                if ($('.logo-img').length > 0 && !$('.logo-img img')[0].naturalWidth > 0) {
                    $('.logo-img img').on('load', function() {
                        window.CUBER.Main._resizeEverything();
                    })
                }

                this.$searchForm = $('.search-bar:not(.no-ajax)');
                this.$searchResults = $('#search-results');
                this.$searchPreloader = $('#site-search .preloader');

                this.$searchForm.find('input[type="search"]').on('keyup', window.debounce((function() {
                    this._ajaxSearchForm();
                }).bind(this), 300));

                this.$searchForm.submit((function(e) {
                    e.preventDefault();
                    this._ajaxSearchForm();
                }).bind(this));

                // tabs & toggles

                if ($('.krown-tabs').data('design') == 'toggles') {

                    $('.krown-tabs').each(function() {

                        var $titles = $(this).children('.titles').children('h5'),
                        $contents = $(this).find('.contents').children('div'),
                        i = 0;

                        $titles.each(function() {

                            $contents.eq(i++).insertAfter($(this));

                            $(this).on('click', function() {

                                var $toggle = $(this).next('.tab');

                                if (!$(this).hasClass('opened')) {
                                    $(this).addClass('opened');
                                    $toggle.stop().slideDown(200);
                                } else {
                                    $(this).removeClass('opened');
                                    $toggle.stop().slideUp(200);
                                }

                            });

                        });

                        $(this).find('.contents').remove();

                    });

                } else {

                    $('.krown-tabs').each(function() {

                        var $titles = $(this).children('.titles').children('h5'),
                        $contents = $(this).children('.contents').children('div'),
                        $openedT = $titles.eq(0),
                        $openedC = $contents.eq(0);

                        $openedT.addClass('opened');
                        $openedC.stop().slideDown(0);

                        $titles.find('a').prop('href', '#').off('click');;

                        $titles.click(function(e) {

                            $openedT.removeClass('opened');
                            $openedT = $(this);
                            $openedT.addClass('opened');

                            $openedC.stop().slideUp(200);
                            $openedC = $contents.eq($(this).index());
                            $openedC.stop().delay(200).slideDown(200);

                            e.preventDefault();

                        });

                    });

                }

                if ($('.site-footer .info').length > 0) {
                    if ($('.site-footer .info').html().split('').length > 18) {
                        $('.site-footer .info').addClass('smaller');
                    }
                }

                if (document.location.href.indexOf('customer_posted=true') > 0) {
                    setTimeout(function() {
                        $('html, body').animate({
                            'scrollTop': $('#contact_form').offset().top
                        }, 1);
                    }, 2000);
                }

            },

            _mountCustomGalleries: function() {

                if ($('#product-gallery-split').length > 0 || $('#collection-gallery-split').length > 0) {

                    window.imagesLoadedLIGHT($('.rte.extract-images'), function() {

                        var gallery = [];

                        $('.rte.extract-images img').each(function() {
                            gallery.push({
                                'src': $(this).attr('src'),
                                'alt': $(this).attr('alt'),
                                'style': $(this)[0].naturalWidth > $(this)[0].naturalHeight ? 'landscape' : 'portrait',
                                'sized': false
                            });
                            $(this).remove();
                        });
                        $('.rte.extract-images p:empty').remove();

                        var galleryDOM = '';

                        gallery.forEach(function(item, key) {

                            var size = 'box--bigger';

                            if (item['style'] == 'landscape') {
                                size = 'box--bigger lap--box--landscape';
                            } else if (item['style'] == 'portrait') {
                                if (item['sized']) {
                                    size = 'box--big lap--box--portrait-small';
                                } else {
                                    if (key + 1 < gallery.length) {
                                        if (gallery[key + 1]['style'] == 'portrait') {
                                            size = 'box--big lap--box--portrait-small';
                                            gallery[key + 1]['sized'] = true;
                                        } else {
                                            size = 'box--bigger lap--box--portrait-large';
                                        }
                                    } else {
                                        size = 'box--bigger lap--box--portrait-large';
                                    }
                                }
                            }

                            var placeholder = item['src'].replace('10x', 'placeholder');
                            if (placeholder.indexOf('placeholder') < 0) {
                                placeholder = placeholder.replace(/(.jpg|.jpeg|.gif|.png)/i, '_placeholder$1');
                            }

                            var set = [
                            placeholder.replace('_placeholder', '_600x'),
                            placeholder.replace('_placeholder', '_860x'),
                            placeholder.replace('_placeholder', '_1100x'),
                            placeholder.replace('_placeholder', '_1600x'),
                            placeholder.replace('_placeholder', '_2100x')
                            ];

                            galleryDOM += '<div class="site-box ' + size + '"><span class="site-box-background with-image" aria-hidden="true"><img class="lazyload" alt="' + item['alt'] + '" src="' + set[0] + '" data-srcset="' + set[0] + ' 480w, ' + set[1] + ' 720w, ' + set[2] + ' 960w, ' + set[3] + ' 1440w, ' + set[4] + ' 1920w" srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-sizes="auto" /></span></div>';
                        });

                        if (galleryDOM != '') {

                            if ($('body').hasClass('template-collection')) {
                                $('#collection-gallery-split').html(galleryDOM);
                            } else if ($('body').hasClass('template-product')) {
                                $('#product-gallery-split').html(galleryDOM);
                            }

                            $('#shopify-section-product').parent().addClass('lift-related-up');
                            $('#collection-gallery-split .site-box, #product-gallery-split .site-box').each(function() {
                                window.CUBER.Main.$siteBoxes.push($(this));
                            });
                            window.CUBER.Main._animateEverything(false);

                            if (window.show_preloader == "true") {
                                $('#collection-gallery-split .lazyload, #product-gallery-split .lazyload').each(function() {
                                    if ($(this).parent().hasClass('site-box-background') || $(this).parent().hasClass('box--product-image')) {
                                        $(this).parent().addClass('show-lazy-preloader');
                                        $(this).on('lazyloaded', function() {
                                            $(this).parent().removeClass('show-lazy-preloader');
                                        });
                                    }
                                });
                            }

                        } else {
                            $('#collection-gallery-split, #product-gallery-split').remove();
                        }


                    });

}

},

_ajaxSearchForm: function() {

    this.$searchPreloader.css('opacity', 1);

    $.ajax({

        type: this.$searchForm.prop('method'),
        url: this.$searchForm.prop('action'),
        data: this.$searchForm.serialize(),
        success: (function(data) {

            this.$searchResults.html($(data).find('.search-results.with-results'));
            this.$searchPreloader.css('opacity', 0);

            $('#site-search').off('scroll.search-pagination');

            if ($('.search-results').find('.next-page').length > 0) {
                this.$searchPagination = $('.search-results').find('.next-page');
                $('#site-search').on('scroll.search-pagination', this._ajaxSearchPagination.bind(this));
            }

        }).bind(this)

    });

},

_ajaxSearchPagination: function() {

    if ($('#site-search').scrollTop() + $(window).height() >= this.$searchPagination.offset().top - 250) {

        this.$searchPreloader.css({
            'opacity': 1,
            'top': 'auto',
            'bottom': '-60px'
        });
        $('#site-search').off('scroll.search-pagination');

        $.ajax({

            url: this.$searchPagination.attr('href'),
            success: (function(data) {

                this.$searchResults.find('.next-page').remove();
                this.$searchResults.append($(data).find('.search-item'));

                if ($(data).find('.search-results .next-page').length > 0) {
                    this.$searchResults.append($(data).find('.search-results .next-page'));
                    this.$searchPagination = $('.search-results').find('.next-page');
                    $('#site-search').on('scroll.search-pagination', this._ajaxSearchPagination.bind(this));
                }

                this.$searchPreloader.css({
                    'opacity': 0,
                    'top': '20px',
                    'bottom': 'auto'
                });

            }).bind(this)

        })

    }

},

_countdownBannerInterval: null,

_countdownBannerInit: function() {

    if (this._countdownBannerInterval) {
        clearInterval(this._countdownBannerInterval);
    }

    if ($('.mount-landing-banner').find('.countdown').length > 0) {

        var $countdown = $('.mount-landing-banner').find('.countdown');

        var $days = $countdown.find('.days'),
        $hours = $countdown.find('.hours'),
        $minutes = $countdown.find('.minutes'),
        $seconds = $countdown.find('.seconds');

        var date = $countdown.data('date').split(','),
        gmt = parseInt($countdown.data('timezone')),
        count = new Date(date[0], (date[1] - 1), date[2], date[3]),
        timezone = count.getTimezoneOffset() / -60;

        if (timezone != gmt) {
            count.setHours(parseInt(date[3]) + timezone - gmt);
        }

        this._countdownBannerInterval = setInterval((function() {

            if ($countdown.hasClass('hide')) {
                $countdown.removeClass('hide');
            }

            var now = new Date().getTime();
            distance = count - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24)),
            hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
            minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
            seconds = Math.floor((distance % (1000 * 60)) / 1000);

            $days.html(days < 10 ? ('0' + days) : days);
            $hours.html(hours < 10 ? ('0' + hours) : hours);
            $minutes.html(minutes < 10 ? ('0' + minutes) : minutes);
            $seconds.html(seconds < 10 ? ('0' + seconds) : seconds);

            if (distance < 0) {
                clearInterval(this._countdownBannerInterval);
                $days.html('00');
                $hours.html('00');
                $minutes.html('00');
                $seconds.html('00');
            }

        }).bind(this), 1000);

    }

},

unmount: function() {
    $(window).off('scroll.scroll-movers');
}

},

Contact: {

    mount: function($elm) {

        if ($elm.find('.contact-map-holder').length > 0) {

            if ($elm.find('.contact-map-object').data('address') != '') {

                if (typeof google !== 'undefined') {
                    this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
                } else {

                    if (window.loadingGoogleMapsScript) {
                        $elm.ti = setInterval((function($elm) {
                            if (typeof google !== 'undefined') {
                                clearInterval($elm.ti);
                                this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
                            }
                        }).bind(this, $elm), 100);

                    } else {

                        window.loadingGoogleMapsScript = true;
                        $.getScript('https://maps.googleapis.com/maps/api/js?v=3&key=' + $elm.find('.contact-map-holder').data('key')).done((function() {
                            this._createMap($elm.attr('id'), $elm.find('.contact-map-object'), $elm.find('.contact-map-address-holder'));
                        }).bind(this));

                    }
                }

            }

        }

    },

    _createMap: function(id, $mapInsert, $mapAddress) {

        $mapInsert.attr('id', 'contact-map-' + id);

        var coordsKey = 'map-coords-' + $mapInsert.attr('id'),
        coordsStorage = localStorage.getItem(coordsKey) != null ? JSON.parse(localStorage.getItem(coordsKey)) : null,
        mapLat = 0,
        mapLong = 0;

        if (coordsStorage != null && coordsStorage['address'] == $mapInsert.data('address')) {
            mapLat = coordsStorage['lat'];
            mapLong = coordsStorage['long'];
        }

        var geocoder, map, address;

        geocoder = new google.maps.Geocoder();
        address = $mapInsert.data('address');

        var mapOptions = {
            zoom: $mapInsert.data('zoom'),
            center: new google.maps.LatLng(mapLat, mapLong),
            streetViewControl: false,
            scrollwheel: true,
            panControl: false,
            mapTypeControl: false,
            overviewMapControl: false,
            zoomControl: false,
            draggable: true,
            styles: $mapInsert.data('style') == 'light' ? window.lightMapStyle : window.darkMapStyle
        };

        map = new google.maps.Map(document.getElementById($mapInsert.attr('id')), mapOptions);

        if (mapLat != 0 || mapLong != 0) {

            var markerOptions = {
                position: new google.maps.LatLng(mapLat, mapLong),
                map: map,
                title: address
            }

            if ($mapInsert.data('marker') != 'none') {
                markerOptions['icon'] = {
                    url: $mapInsert.data('marker'),
                    scaledSize: new google.maps.Size(60, 60)
                }
            }

            $mapAddress.find('a').attr('href', 'http://www.google.com/maps/place/' + mapLat + ',' + mapLong);
            var contentString = $mapAddress.html();
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker(markerOptions);
            marker.addListener('click', function() {
                infowindow.open(map, marker);
                if ($(window).width() < 480) {
                    $('.template-page-contact .box__heading .title').css('marginTop', 50);
                } else if ($(window).width() < 768) {
                    $('.template-page-contact .box__heading .title').css('marginTop', 100);
                }
            });

            if ($(window).width() > 768) {
                map.panBy(-150, 150);
            } else {
                map.panBy(-75, 75);
            }

        } else {

            if (geocoder) {

                geocoder.geocode({
                    'address': address
                }, function(results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {

                            map.setCenter(results[0].geometry.location);

                            var markerOptions = {
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            }

                            if ($mapInsert.data('marker') != 'none') {
                                markerOptions['icon'] = {
                                    url: $mapInsert.data('marker'),
                                    scaledSize: new google.maps.Size(60, 60)
                                }
                            }

                            $mapAddress.find('a').attr('href', 'http://www.google.com/maps/place/' + results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng());
                            var contentString = $mapAddress.html();
                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });

                            var marker = new google.maps.Marker(markerOptions);
                            marker.addListener('click', function() {
                                infowindow.open(map, marker);
                            });

                            if ($(window).width() > 768) {
                                map.panBy(-150, 150);
                            } else {
                                map.panBy(-75, 75);
                            }

                            localStorage.setItem(coordsKey, JSON.stringify({
                                'address': $mapInsert.data('address'),
                                'lat': results[0].geometry.location.lat(),
                                'long': results[0].geometry.location.lng()
                            }));

                        } else {
                            alert("No results found for the given address");
                        }
                    } else {
                        console.log("Geocode was not successful for the following reason: " + status);
                    }

                });

            }

        }

    },

    unmount: function() {

    }

},

        // SCROLL

        Scroll: {

            $body: null,
            $footer: null,

            mount: function() {

                if (!$('html').hasClass('csspositionsticky')) {

                    this.$body = $('body');
                    this.$footer = $('#shopify-section-footer');

                    // grid

                    var scrollFixArray = [];

                    if ($(window).width() > 768) {

                        $('.site-box-container.container--fullscreen.box--can-stick').each(function() {

                            var $boxChildren = $(this).children('.site-box');

                            if ($boxChildren.length == 2) {

                                if ($(this).children('.site-box[data-order="0"]').outerHeight(true) != $(this).children('.site-box[data-order="1"]').outerHeight(true)) {

                                    var $bigChild = null,
                                    $smallChild = null;

                                    if ($(this).children('.site-box[data-order="0"]').outerHeight(true) > $(this).children('.site-box[data-order="1"]').outerHeight(true)) {
                                        $bigChild = $(this).children('.site-box[data-order="0"]');
                                        $smallChild = $(this).children('.site-box[data-order="1"]');
                                    } else if ($(this).children('.site-box[data-order="1"]').outerHeight(true) > $(this).children('.site-box[data-order="0"]').outerHeight(true)) {
                                        $bigChild = $(this).children('.site-box[data-order="1"]');
                                        $smallChild = $(this).children('.site-box[data-order="0"]');
                                    }

                                    scrollFixArray.push({
                                        'parent': $(this),
                                        'big-child': $bigChild,
                                        'small-child': $smallChild
                                    });

                                }

                            } else if ($boxChildren.length == 1) {

                                if ($(this).children('.site-box[data-order="0"]').outerHeight(true) > $(window).height()) {

                                    scrollFixArray.push({
                                        'parent': $(this),
                                        'big-child': $(this).children('.site-box[data-order="0"]'),
                                        'small-child': null
                                    });

                                }

                            }

                        });

                    }

                    scrollFixArray.forEach(function(obj) {

                        obj['parent'].removeClass('fixing-scroll-now');
                        obj['big-child'].css({
                            'position': 'relative',
                            'top': '0',
                            'bottom': 'auto',
                            'marginLeft': '0'
                        });

                        if (obj['small-child'] != null) {
                            obj['small-child'].css({
                                'position': 'relative',
                                'top': '0',
                                'bottom': 'auto',
                                'marginLeft': '0'
                            });
                            obj['small-child'].removeClass('ok-scroll');
                        }


                    });

                    $(window).off('scroll.scroll-fix');

                    scrollFixArray.forEach(function(obj) {

                        if ($(window).scrollTop() + $(window).height() >= obj['parent'].offset().top + obj['parent'].outerHeight()) {

                            if (obj['small-child'] != null) {
                                obj['small-child'].css({
                                    'position': 'absolute',
                                    'bottom': 0,
                                    'top': 'auto'
                                });
                                if (obj['small-child'].attr('data-order') == '1') {
                                    obj['small-child'].css('marginLeft', '50%');
                                }
                            }

                            if (obj['big-child'].attr('data-order') == '1') {
                                obj['big-child'].css('marginLeft', '50%');
                            }

                        }

                    })

                    $(window).on('scroll.scroll-fix', (function() {

                        if ($(window).scrollTop() >= 0) {

                            scrollFixArray.forEach(function(obj) {

                                if ($(window).scrollTop() >= obj['parent'].offset().top && $(window).scrollTop() + $(window).height() < obj['parent'].offset().top + obj['parent'].outerHeight() && !obj['parent'].hasClass('fixing-scroll-now')) {

                                    obj['parent'].addClass('fixing-scroll-now');

                                    if (obj['small-child'] != null) {

                                        obj['small-child'].css({
                                            'position': 'fixed',
                                            'top': 0,
                                            'bottom': 'auto'
                                        });

                                        if (obj['small-child'].attr('data-order') == '1') {
                                            obj['small-child'].css('marginLeft', '50%');
                                        }

                                        if (obj['small-child'].height() > $(window).height()) {
                                            obj['small-child'].addClass('ok-scroll');
                                        }

                                    }

                                    if (obj['big-child'].attr('data-order') == '1') {
                                        obj['big-child'].css('marginLeft', '50%');
                                    }

                                }

                                if ($(window).scrollTop() + $(window).height() >= obj['parent'].offset().top + obj['parent'].outerHeight() && obj['parent'].hasClass('fixing-scroll-now')) {

                                    obj['parent'].removeClass('fixing-scroll-now');

                                    if (obj['small-child'] != null) {
                                        obj['small-child'].removeClass('ok-scroll');
                                        obj['small-child'].css({
                                            'position': 'absolute',
                                            'bottom': 0,
                                            'top': 'auto'
                                        });
                                    }

                                }

                                if ($(window).scrollTop() < obj['parent'].offset().top && obj['parent'].hasClass('fixing-scroll-now')) {

                                    obj['parent'].removeClass('fixing-scroll-now');
                                    obj['big-child'].css('marginLeft', '0');

                                    if (obj['small-child'] != null) {
                                        obj['small-child'].css({
                                            'position': 'relative',
                                            'top': '0',
                                            'bottom': 'auto',
                                            'marginLeft': '0'
                                        });
                                    }


                                }
                                if (obj['small-child'] != null && obj['small-child'].height() > $(window).height() && obj['small-child'].hasClass('ok-scroll')) {

                                    var x = obj['big-child'].height() - $(window).height(),
                                    y = $(window).height() - obj['small-child'].height(),
                                    z = $(window).scrollTop();

                                    //obj['small-child'].css('top', Math.ceil(z * y / x));

                                }


                            });

                        }

                    }).bind(this)).trigger('scroll.scroll-fix');

                    $(window).off('resize.scroll-fix');
                    $(window).on('resize.scroll-fix', window.debounce((function() {
                        window.CUBER.Scroll.mount();
                    }).bind(this), 250));

                }

            },

            unmount: function() {

                $(window).off('resize.scroll-fix');
                $(window).off('scroll.scroll-fix');

            }

        },

        // IMAGES

        Images: {

            boxImages: [],

            mount: function() {

                this.boxImages = [];

                if ($('.box--product-image').length > 0) {
                    $('.box--product-image:not(.on)').each((function(key, elm) {

                        $(elm).addClass('on');
                        this.boxImages.push({
                            '$elm': $(elm),
                            'img': $(elm).children('img')[0],
                            'srcset': $(elm).children('img').data('srcset')
                        });

                    }).bind(this));
                }

                $(window).on('resize.box-images', (function() {

                    this.boxImages.forEach(function(entry) {

                        var desiredDensity = window.devicePixelRatio <= 2 ? window.devicePixelRatio : 2,
                        desiredSize = entry['$elm'].width() * desiredDensity;

                        if (entry['img'].naturalWidth > 0) {

                            if (entry['$elm'].width() / entry['$elm'].height() < entry['img'].naturalWidth / entry['img'].naturalHeight) {
                                var desiredHeight = Math.ceil((entry['$elm'].height() * desiredDensity));
                                desiredSize = desiredHeight * entry['img'].naturalWidth / entry['img'].naturalHeight;
                            }

                        }

                        if (desiredSize <= 480) {
                            entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].small + ')');
                        } else if (desiredSize <= 960) {
                            entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].medium + ')');
                        } else if (desiredSize <= 1440) {
                            entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].large + ')');
                        } else if (desiredSize > 1440) {
                            entry['$elm'].css('backgroundImage', 'url(' + entry['srcset'].huge + ')');
                        }

                    });

                }).bind(this)).trigger('resize.box-images');

            },

            unmount: function() {
                $(window).off('resize.box-images');
            }

        },

        Video: {

            mount: function() {

                if ($('.video-lightbox').length > 0) {
                    $('.video-lightbox').magnificPopup({
                        type: 'iframe',
                        iframe: {
                            patterns: {
                                youtube: {
                                    index: 'youtube.com',
                                    id: 'v=',
                                    src: '//www.youtube.com/embed/%id%?rel=0&autoplay=1'
                                }
                            }
                        }
                    });
                }

            },

            unmount: function() {}

        },

        VideoBackground: {

            backgroundPlayers: {},

            mount: function() {

                if ($('.mount-video-background .site-box-video-background .player').length > 0 && $('html').hasClass('no-touchevents')) {

                    if (typeof YT == 'undefined') {
                        var tag = document.createElement('script');
                        tag.src = "https://www.youtube.com/iframe_api";
                        var firstScriptTag = document.getElementsByTagName('script')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                    }

                    $('.mount-video-background').each((function(key, elm) {
                        this.backgroundPlayers[$(elm).find('.site-box-video-background .player').attr('id')] = {
                            player: null,
                            selector: $(elm).find('.site-box-video-background .player').attr('id'),
                            video: $(elm).find('.site-box-video-background').attr('data-video')
                        };
                    }).bind(this));

                    // don't need YT, using S3

                    // window.onYouTubeIframeAPIReady = (function() {
                    //
                    // 	for ( var key in this.backgroundPlayers ) {
                    //
                    // 		if ( this.backgroundPlayers.hasOwnProperty(key) ) {
                    // 			var item = this.backgroundPlayers[key];
                    // 			item['player'] = new YT.Player(item['selector'], {
                    // 	    	height: '1920',
                    // 	      width: '1080',
                    // 	      videoId: item['video'],
                    // 	      playerVars: { 'autoplay': 1, 'controls': 0, 'showinfo': 0, 'rel': 0, 'enablejsapi':1, 'wmode' : 'transparent'},
                    // 	      events : {
                    // 	     	 	'onReady' : CUBER.VideoBackground._pkOnPlayerReady.bind(this),
                    // 	        'onStateChange' : CUBER.VideoBackground._pkOnPlayerStateChange.bind(this)
                    // 	      }
                    // 	  	});
                    //
                    // 		}
                    // 	}
                    //
                    // }).bind(this);

                    setTimeout(function() {
                        $('.site-box-video-background').fadeIn();
                    }, 2000);

                }

            },

            unmount: function() {},

            _pkOnPlayerStateChange: function(e) {
                var frm = $(e.target.getIframe());
                if (e.data === YT.PlayerState.PLAYING) {
                    this.backgroundPlayers[frm.attr('id')]['player'].setPlaybackQuality('hd1080');
                }
                if (e.data === YT.PlayerState.ENDED) {
                    this.backgroundPlayers[frm.attr('id')]['player'].playVideo();
                }
            },

            _pkOnPlayerReady: function(e) {
                var frm = $(e.target.getIframe());
                this.backgroundPlayers[frm.attr('id')]['player'].mute();
                this.backgroundPlayers[frm.attr('id')]['player'].playVideo();
            },


        },

        Banner: {

            $banner: null,

            _bannerKey: null,
            _bannerStorage: null,
            _bannerText: '',

            mount: function() {

                this.$banner = $('.shopify-section.mount-banner');

                this._bannerKey = this.$banner.attr('id');
                this._bannerStorage = !localStorage.getItem(this._bannerKey) ? 'empty' : JSON.parse(localStorage.getItem(this._bannerKey));
                this._bannerText = this._extractText(this.$banner.find('.box__banner .content').html());

                var show = this.$banner.find('.box__banner').data('show');

                // alternate for specific pages
                if ($('body').hasClass('template-collection') && this.$banner.find('.box__banner').data('collection') != '' && this.$banner.find('.box__banner').data('collection') != $('body').attr('id')) {
                    show = false;
                } else if ($('body').hasClass('template-product') && this.$banner.find('.box__banner').data('product') != '' && this.$banner.find('.box__banner').data('product') != $('body').attr('id')) {
                    show = false;
                }

                if (show && (this._bannerStorage == 'empty' || this._bannerStorage['content'] != this._bannerText)) {

                    setTimeout((function() {
                        this._show(this.$banner);
                    }).bind(this), 400);

                    this.$banner.find('.close').on('click', (function() {
                        this._hide(this.$banner, true);
                    }).bind(this));

                }

            },

            _show: function($banner) {
                $banner.addClass('active');
            },

            _hide: function($banner, remember) {

                $banner.removeClass('active');

                if (remember) {
                    localStorage.setItem(this._bannerKey, JSON.stringify({
                        'shown': 'yes',
                        'content': this._bannerText
                    }));
                }

            },

            _extractText: function(content) {
                var span = document.createElement('span');
                span.innerHTML = content;
                return [span.textContent || span.innerText].toString().replace(/ +/g, '');
            }

        },

        Popup: {

            $popup: null,

            mount: function() {

                this.$popup = $('#shopify-section-popup');

                var show = this.$popup.find('.popup-content').data('show'),
                freq = this.$popup.find('.popup-content').data('freq'),
                enable = this.$popup.find('.popup-content').data('enable');

                if (show > 0 && enable) {
                    setTimeout((function() {
                        this._trigger(show, freq);
                    }).bind(this), parseInt(show * 1000));
                }

                this.$popup.find('.site-close-handle').on('click', (function(e) {
                    this._hide();
                }).bind(this));

                this.$popup.find('.popup-background').on('click', (function(e) {
                    this._hide();
                }).bind(this));

            },

            _show: function() {
                this.$popup.addClass('active');
            },

            _hide: function() {
                this.$popup.removeClass('active');
            },

            _trigger: function(show, freq) {

                var popupKey = 'popup-' + document.location.hostname,
                popupStorage = localStorage.getItem(popupKey) != null ? JSON.parse(localStorage.getItem(popupKey)) : null;

                if (popupStorage != null) {

                    if (popupStorage['show'] != show || popupStorage['freq'] != freq) {

                        this._refreshStorage(popupKey, show, freq);;

                        // user saw the ad but settings are different - show it!
                        this._show();

                    } else {

                        // user saw the ad so we need to check if he should see it again

                        if (freq == 'every') {

                            if (sessionStorage[popupKey] == null || sessionStorage[popupKey] != 'shown') {
                                this._show();
                                this._refreshStorage(popupKey, show, freq);;
                            }

                        } else {

                            var shownAt = popupStorage['shown'],
                            nowAt = new Date().getTime(),
                            inBetween = Math.round((nowAt - shownAt) / 1000);

                            if (freq == 'day' && inBetween > 129600) {
                                this._show();
                                this._refreshStorage(popupKey, show, freq);;
                            } else if (freq == 'week' && inBetween > 907200) {
                                this._show();
                                this._refreshStorage(popupKey, show, freq);;
                            }

                        }

                    }

                } else {

                    this._refreshStorage(popupKey, show, freq);

                    // user never saw the ad - show it!

                    this._show();

                }

            },

            _refreshStorage: function(popupKey, show, freq) {

                localStorage.setItem(popupKey, JSON.stringify({
                    'show': show,
                    'freq': freq,
                    'shown': new Date().getTime()
                }));

                sessionStorage[popupKey] = 'shown';

            },

            unmount: function() {}

        },

        // SOCIAL

        Social: {

            mount: function($elm) {

                var $container = $elm.children('.site-box-container');
                var nameTwitter = $container.data('twitter');
                var nameInstagram = $container.data('instagram');

                $container.data('checkTwitter', 'false');
                $container.data('checkInstagram', 'false');

                var no = $container.data('posts'),
                noT = 0,
                noI = 0,
                socialArr = [];

                if (nameTwitter != '' && nameInstagram != '') {
                    noT = Math.ceil(no / 2);
                    noI = Math.floor(no / 2);
                } else if (nameTwitter != '') {
                    noT = no;
                } else if (nameInstagram != '') {
                    noI = no;
                }

                if (noI > 0) {

                    var feed = new Instafeed({

                        get: 'user',
                        userId: nameInstagram.split('.')[0],
                        accessToken: nameInstagram,
                        limit: noI,
                        resolution: 'standard_resolution',
                        mock: true,
                        sortBy: 'random',
                        success: (function(obj) {

                            obj.data.forEach(function(entry) {

                                socialArr.push({
                                    'type': 'instagram',
                                    'thumb': entry.images.standard_resolution.url,
                                    'link': entry.link,
                                    'caption': typeof entry.caption === 'object' && entry.caption !== null ? entry.caption.text : '',
                                    'likes': entry.likes.count,
                                    'time': new Date(entry.created_time * 1000).toLocaleDateString('en-US'),
                                    'timestamp': parseInt(entry.created_time)
                                });
                            });

                            this._pushFeed('instagram', socialArr, nameTwitter, $container);

                        }).bind(this)

                    });

                    feed.run();

                } else {
                    this._pushFeed('instagram', socialArr, nameTwitter, $container);
                }

                if (noT > 0) {

                    twitterFetcher.fetch({

                        'profile': {
                            'screenName': nameTwitter
                        },
                        'dataOnly': true,
                        'maxTweets': noT,
                        'dateFunction': function(date) {
                            return parse(date);
                        },
                        'customCallback': (function(obj) {

                            obj.forEach(function(entry) {
                                socialArr.push({
                                    'type': 'twitter',
                                    'text': entry.tweet,
                                    'timestamp': Date.parse(entry.timestamp) / 1000
                                })
                            });

                            this._pushFeed('twitter', socialArr, nameTwitter, $container);

                        }).bind(this)

                    });

                } else {
                    this._pushFeed('twitter', socialArr, nameTwitter, $container);
                }

            },

            _pushFeed: function(type, feed, nameTwitter, $container) {

                if (type == 'twitter') {
                    $container.data('checkTwitter', 'true');
                } else if (type == 'instagram') {
                    $container.data('checkInstagram', 'true');
                }

                if ($container.data('checkTwitter') == 'true' && $container.data('checkInstagram') == 'true') {

                    feed.sort(function(a, b) {
                        return a.timestamp - b.timestamp;
                    }).reverse();

                    feed.forEach((function(entry, key) {

                        var feedContent = '';

                        if (entry.type == 'twitter') {

                            feedContent += '<div class="site-box-content">';
                            feedContent += '<span class="icon" aria-hidden="true">' + $.themeAssets.iconTwitter + '</span>';
                            feedContent += '<p>' + entry.text + '</p>';
                            feedContent += '<a href="https://twitter.com/' + nameTwitter + '" target="_blank">@' + nameTwitter + '</a>';
                            feedContent += '</div>';

                        } else if (entry.type == 'instagram') {

                            feedContent += '<div class="site-box-content" style="background-image:url(' + entry.thumb + ')">';
                            feedContent += '<a href="' + entry.link + '" target="_blank">';
                            feedContent += '<span class="icon child" aria-hidden="true">' + $.themeAssets.iconInstagram + '</span>';
                            feedContent += '<p class="caption child">' + entry.caption + '</p>';
                            feedContent += '<div class="meta child">';
                            feedContent += '<span class="likes">' + $.themeAssets.iconHeart + entry.likes + '</span>';
                            feedContent += '<span class="time">' + entry.time + '</span>';
                            feedContent += '</div>';
                            feedContent += '</a>';
                            feedContent += '</div>';

                        }

                        $container.find('.site-box[data-order="' + (key + 1) + '"]')
                        .removeClass('box__twitter').removeClass('box__instagram')
                        .addClass('box__' + entry.type)
                        .html(feedContent);

                    }).bind(this));

                }

            },

            unmount: function() {
                //
            }

        },

        SplitSlider: {

            mount: function($section) {
                if (!$section.find('.box__slideshow-split').hasClass('one-image')) {
                    $section.KrownSplitScreenSlider();
                } else {
                    $(window).on('resize.split-screen-single', function() {
                        $section.find('.box__slideshow-split').height($(window).height() - $('#site-header').outerHeight(true)).addClass('remove-min-height');
                    }).trigger('resize.split-screen-single');
                }
            },

            unmount: function($section) {
                $(window).off('scroll.splid-slider-' + $section.attr('id'));
            }

        }

    }

    $(document).on('ready', function() {

        window.CUBER.Nav.mount();
        window.CUBER.Main.mount();
        window.CUBER.Scroll.mount();

        $('.contact-nwd-open-popup').magnificPopup({
            type: 'inline',
            midClick: true
        });

        if ($('.mount-social').length > 0) {
            $('.mount-social').each(function() {
                window.CUBER.Social.mount($(this));
            })
        }

        if ($('.mount-slideshow').length > 0) {
            $('.mount-slideshow').each(function() {
                if ($(this).find('.box__slideshow-split').length > 0) {
                    window.CUBER.SplitSlider.mount($(this));
                }
            })
        }

        if ($('.mount-product').length > 0) {
            $('.mount-product').each(function() {
                window.CUBER.Product.mount($(this));
            });
        }
        if ($('body').hasClass('template-collection')) {
            window.CUBER.Collection.mount();
        }
        if ($('body').hasClass('template-page-contact') || ($('body').hasClass('template-index')) && $('.mount-map').length > 0) {
            $('.mount-map').each(function() {
                window.CUBER.Contact.mount($(this));
            });
        }

        if ($('.mount-video-background').length > 0) {
            window.CUBER.VideoBackground.mount();
        }

        if ($('.mount-landing-banner').length > 0) {
            window.CUBER.Main._countdownBannerInit();
        }

        window.CUBER.Video.mount();
        window.CUBER.Popup.mount();
        window.CUBER.Banner.mount();

        // Section events

        // select

        $(document).on('shopify:section:select', function(e) {

            var $section = $(e.target);

            if ($section.hasClass('mount-header')) {

                if ($section.find('#site-header').hasClass('style--sidebar') || $section.find('#site-header').hasClass('style--fullscreen')) {
                    if (!$section.find('#site-nav').hasClass('active')) {
                        $('#site-menu-handle a').trigger('click');
                    }
                }

            }

            if ($('#site-header').hasClass('desktop-view--minimal') && $('#fix-me-header').css('display') == 'none') {
                setTimeout(function() {
                    $('html, body').stop().animate({
                        'scrollTop': $section.offset().top
                    }, 0);
                    //console.log('culprit?');
                }, 400);
            }

            if ($section.hasClass('mount-popup')) {
                window.CUBER.Popup._show();
            }
            if ($section.hasClass('mount-banner')) {
                window.CUBER.Banner._show($section);
            }

        });

        // deselect

        $(document).on('shopify:section:deselect', function(e) {

            var $section = $(e.target);

            if ($section.hasClass('mount-header')) {

                if ($section.find('#site-header').hasClass('style--sidebar') || $section.find('#site-header').hasClass('style--fullscreen')) {
                    if ($section.find('#site-nav').hasClass('active')) {
                        $('#site-menu-handle a').trigger('click');
                    }
                }

            }

            if ($section.hasClass('mount-popup')) {
                window.CUBER.Popup._hide();
            }
            if ($section.hasClass('mount-banner')) {
                window.CUBER.Banner._hide($section, false);
            }


        });

        // load

        $(document).on('shopify:section:load', function(e) {

            var $section = $(e.target);

            if ($section.hasClass('mount-header')) {
                window.CUBER.Nav.mount();
            }
            if ($section.hasClass('mount-images')) {
                //window.CUBER.Images.mount();
            }
            if ($section.hasClass('mount-video')) {
                window.CUBER.Video.mount();
            }
            if ($section.hasClass('mount-video-background')) {
                window.CUBER.VideoBackground.mount();
            }
            if ($section.hasClass('mount-social')) {
                window.CUBER.Social.mount($section);
            }
            if ($section.hasClass('mount-slideshow') && $section.find('.box__slideshow-split').length > 0) {
                window.CUBER.SplitSlider.mount($section);
                $(window).scrollTop(0);
            }
            if ($section.hasClass('mount-product')) {
                window.CUBER.Product.mount($section);
            }
            if ($section.hasClass('mount-map')) {
                window.CUBER.Contact.mount($section);
            }

            if ($section.hasClass('mount-popup')) {
                window.CUBER.Popup.mount();
            }
            if ($section.hasClass('mount-banner')) {
                window.CUBER.Banner.mount();
            }
            if ($section.hasClass('mount-gallery')) {
                window.CUBER.Main._mountCustomGalleries();
            }

            if ($section.hasClass('mount-landing-banner')) {
                window.CUBER.Main._countdownBannerInit();
            }

            window.CUBER.Main.mount();
            window.CUBER.Scroll.mount();

        });

        // unload

        $(document).on('shopify:section:unload', function(e) {

            var $section = $(e.target);

            if ($section.hasClass('mount-header')) {
                window.CUBER.Nav.unmount();
            }
            if ($section.hasClass('mount-images')) {
                window.CUBER.Images.unmount();
            }
            if ($section.hasClass('mount-video')) {
                window.CUBER.Video.unmount();
            }
            if ($section.hasClass('mount-social')) {
                window.CUBER.Social.unmount();
            }
            if ($section.hasClass('mount-slideshow') && $section.find('.box__slideshow-split').length > 0) {
                window.CUBER.SplitSlider.unmount($section);
            }
            if ($section.hasClass('mount-product')) {
                window.CUBER.Product.unmount($section);
            }
            if ($section.hasClass('mount-map')) {
                window.CUBER.Contact.unmount();
            }

            window.CUBER.Main.unmount();
            window.CUBER.Scroll.unmount();

        });

        // blocks

        $(document).on('shopify:block:select', function(e) {

            var $block = $(e.target);

            if ($block.hasClass('slideshow-item')) {
                if ($block.closest('.responsive-flickity').hasClass('flickity-enabled')) {
                    $block.closest('.responsive-flickity').data('flickity').select($block.index())
                } else {
                    $(window).scrollTop($block.offset().top - $('#site-header').outerHeight());
                    // console.log('here?');
                }
                $block.find('.caption, .title, .subtitle').css('transform', 'translateY(0)');
            } else if ($block.hasClass('box__collection-footer')) {
                window.CUBER.Main._mountCustomFooter($block);
            }

        });

        // Various stuff that doesn't need remounting

        $(window).on('resize', function() {
            if ($(window).width() < 1024) {

            }
        });

        $('.simple-grid select:not(.styled)').each(function() {
            $(this).styledSelect({
                coverClass: 'regular-select-cover',
                innerClass: 'regular-select-inner'
            }).addClass('styled');
            $(this).parent().append($.themeAssets.arrowDown);
        });

        if ($('body').hasClass('template-customers-login')) {

            if ($('#RecoverPassword').length > 0) {

                $('#RecoverPassword').on('click', function(e) {
                    $('#CustomerLoginForm').hide();
                    $('#RecoverPasswordForm').show();
                    $('#LoginRecoverTitle').html($('#LoginRecoverTitle').data('password_recovery'));
                    e.preventDefault();
                });

                $('#HideRecoverPasswordLink').on('click', function(e) {
                    $('#RecoverPasswordForm').hide();
                    $('#CustomerLoginForm').show();
                    $('#LoginRecoverTitle').html($('#LoginRecoverTitle').data('login'));
                    e.preventDefault();
                });

                if (window.location.hash == '#recover') {
                    $('#RecoverPassword').trigger('click');
                }

            }

        } else if ($('body').hasClass('template-customers-addresses')) {
            $('#section-addresses a').on('click', function() {
                window.CUBER.Scroll.mount();
            })
        }

        //

        if ($('body').hasClass('template-blog') || $('body').hasClass('template-article')) {

            window.CUBER.Main._mountScrollMovers({
                'parent': $('.scroll-movers-parent'),
                'items': $('.site-sharing')
            });

            window.CUBER.Main._mobileSharingInit();

        }

    });

})(jQuery);

function response(data) {
    console.log(data);
}

window._getLuminance = function(hexy) {
    var rgb = this._toRgb(hexy);
    return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
}

window._toRgb = function(hexy) {
    var hex = hexy.replace('rgb(', '');
    hex = hex.replace(')', '');
    hex = hex.replace(' ', '');
    hex = hex.replace(' ', '');
    hex = hex.split(',');
    return {
        r: hex[0],
        g: hex[1],
        b: hex[2]
    };
}

$(window).on('load', function() {
    setTimeout(function() {
        if ($('body').attr('id') == 'challenge' || location.pathname == '/challenge') {
            document.location.hash = 'challenge';
            $('html, body').scrollTop(0);
            // console.log('scroll?');
        }
    }, 150);
});

if (navigator.userAgent.match(/instagram/i)) {

    var screenHeight = window.screen.height - 100,
    vh100 = screenHeight + 'px',
    vh80 = screenHeight * .8 + 'px',
    vh75 = screenHeight * .75 + 'px',
    vh70 = screenHeight * .7 + 'px',
    vh60 = screenHeight * .6 + 'px',
    vh50 = screenHeight * .5 + 'px',
    vh45 = screenHeight * .45 + 'px',
    vh40 = screenHeight * .40 + 'px',
    vh33 = screenHeight * .33 + 'px',
    vh10 = screenHeight * .1 + 'px',
    vh164 = screenHeight * 1.64 + 'px';

    var stupidBrowserStyle = '.box--small {height: ' + vh50 + ' !important;}.box--small-lg {height: ' + vh50 + ' !important;}.box--small-fl {height: ' + vh50 + ' !important;}.box--big {min-height: ' + vh100 + ';}.box--bigger {min-height: ' + vh100 + ';}@media screen and (max-width: 1024px) {.portable--box--small {height: ' + vh50 + ' !important;}.portable--box--small-lg {height: ' + vh50 + ' !important;}.portable--box--small-fl {height: ' + vh50 + ' !important;}.portable--box--big {min-height: ' + vh100 + ';}.portable--box--bigger {min-height: ' + vh100 + ';}}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.lap--box--small {height: ' + vh50 + ' !important;}.lap--box--small-lg {height: ' + vh50 + ' !important;}.lap--box--small-fl {height: ' + vh50 + ' !important;}.lap--box--big {min-height: ' + vh100 + ';}}@media screen and (max-width: 480px) {.palm--box--small {height: ' + vh50 + ' !important;}.palm--box--small-lg {height: ' + vh50 + ' !important;}.palm--box--small-fl {height: ' + vh50 + ' !important;}.palm--box--big {min-height: ' + vh100 + ';}.palm--box--bigger {min-height: ' + vh100 + ';}}.site-box.box__blog .blog-item {height: ' + vh50 + ';}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.template-collection .site-box.box__heading.box--big:not(.fix-me-with-height-hard).lap--box--small-fl {min-height: ' + vh50 + ' !important;}}.site-box.box__heading.box--big,.site-box.box__heading.box--bigger {height: ' + vh100 + ';}@media screen and (min-width: 481px) {.mount-testimonials .site-box-container:not([data-all_posts="2"]) .site-box {height: auto !important;min-height: ' + vh50 + ' !important;}.mount-testimonials .site-box-container[data-all_posts="1"] .site-box {min-height: ' + vh100 + ' !important;}}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.site-box-container[data-posts="0"] .site-box.box__testimonial:last-child {min-height: ' + vh50 + ' !important;}}@media screen and (max-width: 480px) {.site-box.box__testimonial {min-height: ' + vh33 + ' !important;}}@media screen and (min-width: 481px) {.mount-testimonials .site-box-container:not([data-all_posts="2"]) .ie9 .site-box.box__testimonial {height: ' + vh50 + ' !important;}}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.box__map .contact-map-holder {height: ' + vh50 + ';}}@media screen and (max-width: 768px) and (orientation: landscape) {.mount-social .box__heading {height: ' + vh50 + ' !important;}}.site-footer-size--sm .box__footer {min-height: ' + vh50 + ' !important;}@media screen and (max-width: 768px) and (orientation: landscape) {.site-box.box__collection {height: ' + vh100 + ' !important;}}.site-box.box__collection.box--big .product-item {height: ' + vh100 + ';}.template-collection .site-box.box__heading.box--big:not(.fix-me-with-height-hard) {min-height: ' + vh100 + ' !important;}@media screen and (max-width: 768px) and (orientation: landscape) {.site-box.box__heading {height: ' + vh80 + ' !important;}.mount-products .site-box.box__heading {height: ' + vh100 + ' !important;}}.site-box.box__slideshow-split .slideshow-item .site-box.box--big.box--big,.site-box.box__slideshow-split .slideshow-item .site-box.box--big.box--bigger {height: ' + vh100 + ';}.site-box.box__slideshow-split .slideshow-item .site-box-background-container, .site-box.box__slideshow-split .slideshow-item .site-box-background-container .site-box-background {height: ' + vh100 + ';}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.site-box.box__slideshow-split {height: calc(' + vh100 + ' - 91px);}}.site-box.box__image-text.box--big,.site-box.box__image-text.box--bigger {height: ' + vh100 + ';}.site-nav.style--sidebar {height: ' + vh100 + ';}.site-overlay {height: ' + vh100 + ';}@media screen and (max-width: 1024px) {.box__product-gallery.scroll .site-box-content {height: ' + vh100 + ';}}.box__product-gallery.slider .site-box-content {height: ' + vh100 + ';}.gallery-item {height: ' + vh100 + ';}@media screen and (orientation: landscape) {.expand .gallery-item {height: ' + vh100 + ';}}#product-zoomed-image {height: calc(' + vh100 + ' + 20px);}.box__map .map-info {max-height: ' + vh70 + ';}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait) {.box__product-gallery {height: ' + vh70 + ' !important;}}@media screen and (max-width: 768px) and (orientation: landscape) {.box__product-gallery {height: ' + vh164 + ' !important;}}html .template-collection-lookbook {min-height: ' + vh50 + ' !important;}@media screen and (max-width: 768px),screen and (max-width: 1024px) and (orientation: portrait){ #collection-gallery .lap--box--portrait-small, #section-product_extra .lap--box--portrait-small {height: ' + vh40 + ';} #collection-gallery .lap--box--portrait-large, #section-product_extra .lap--box--portrait-large {height: ' + vh75 + ';} #collection-gallery .lap--box--landscape, #section-product_extra .lap--box--landscape {height: ' + vh45 + ';} .box__collection-footer, .box__video-background {min-height: ' + vh50 + ' !important; padding: ' + vh10 + ';} } .box__landing-banner {min-height: ' + vh60 + '; padding: ' + vh10 + ' 0;} .landing-page-content {min-height: ' + vh50 + ' !important; padding: ' + vh10 + ';} .box__next-collection {height: ' + vh40 + ' !important;}';

    $('head').append('<style type="text/css">' + stupidBrowserStyle + '</style>');

}

$(document).ready(function(){
    if($(".template-collection").length > 0){
        $('.extra-links .remove-item-data').on('click', function(e) {
            debugger;
            e.preventDefault();
            if ($(this).closest("#step-2").length > 0) {
                console.log('remove ring');
                Cookies.remove('ring_handle');
                Cookies.remove('ring_variant');
                Cookies.remove('ring_title');
                Cookies.remove('ring_price');
                Cookies.remove('ring_image');
                Cookies.remove('ring_alt');
                Cookies.remove('ring_video');
                Cookies.remove('ring_style');
                var ring_handle = (Cookies.get('ring_handle') != undefined) ? Cookies.get('ring_handle') : 'null';
                var ring_style = (Cookies.get('ring_variant') != undefined) ? Cookies.get('ring_variant') : 'null';
                var url = '/collections/all-lab-grown-diamonds?ring=' + ring_handle + '&style=' + ring_style;
            } else {
                console.log('remove diamond');
                Cookies.remove('diamond_id');
                Cookies.remove('diamond_handle');
                Cookies.remove('diamond_title');
                Cookies.remove('diamond_price');
                Cookies.remove('diamond_image');
                Cookies.remove('diamond_video');

                var diamond_handle = (Cookies.get('diamond_handle') != undefined) ? Cookies.get('diamond_handle') : "null";
                var url = '/collections/engagement-ring-settings?diamond=' + diamond_handle;
            }
            window.location.href = url;
        });
    }
});